(self["webpackChunkRamkyPOPR"] = self["webpackChunkRamkyPOPR"] || []).push([["src_app_home_home_module_ts"],{

/***/ 52003:
/*!*********************************************!*\
  !*** ./src/app/home/home-routing.module.ts ***!
  \*********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HomePageRoutingModule": function() { return /* binding */ HomePageRoutingModule; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./home.page */ 62267);




const routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_0__.HomePage,
    }
];
let HomePageRoutingModule = class HomePageRoutingModule {
};
HomePageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule]
    })
], HomePageRoutingModule);



/***/ }),

/***/ 3467:
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HomePageModule": function() { return /* binding */ HomePageModule; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./home.page */ 62267);
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./home-routing.module */ 52003);







let HomePageModule = class HomePageModule {
};
HomePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _home_routing_module__WEBPACK_IMPORTED_MODULE_1__.HomePageRoutingModule
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_0__.HomePage]
    })
], HomePageModule);



/***/ }),

/***/ 62267:
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HomePage": function() { return /* binding */ HomePage; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_home_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./home.page.html */ 49764);
/* harmony import */ var _home_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./home.page.scss */ 2610);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _services_hero_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/hero.service */ 19405);







let HomePage = class HomePage {
    constructor(heroService, toastController, router, loadingController) {
        this.heroService = heroService;
        this.toastController = toastController;
        this.router = router;
        this.loadingController = loadingController;
        this._poCount = 0;
        this._prCount = 0;
    }
    ngOnInit() {
        // this.getPending_counts();
    }
    ionViewDidEnter() {
        this.getPending_counts();
    }
    getPending_counts(refresher) {
        let that = this;
        let user;
        if (localStorage.getItem("SAPIDS") != null) {
            user = localStorage.getItem("SAPIDS");
        }
        else {
            user = null;
        }
        console.log("check SAP IDs: ", user);
        let dataObj = {
            _name: "SOAP:Envelope",
            _attrs: {
                "xmlns:SOAP": "http://schemas.xmlsoap.org/soap/envelope/"
            },
            _content: {
                "SOAP:Body": [
                    {
                        _name: "GetPendingCounts",
                        _attrs: {
                            "xmlns": "http://schemas.cordys.com/PRPOApprovalsWSApp"
                        },
                        _content: [
                            { "SAPID": user }
                        ]
                    }
                ]
            }
        };
        that.loadingController.create({
            spinner: 'bubbles',
            message: 'Please wait...'
        }).then((loadEl) => {
            loadEl.present();
            that.heroService.testService123(dataObj, function (err, response) {
                loadEl.dismiss();
                debugger;
                if (refresher != undefined) {
                    refresher.target.complete();
                }
                if (response) {
                    let obj = $.cordys.json.findObjects(response, "getPendingCounts");
                    console.log("check search obj: ", obj);
                    if (obj != undefined) {
                        that._poCount = obj[0].getPendingCounts[0].tuple[0].PO_COUNT[0];
                        that._prCount = obj[0].getPendingCounts[0].tuple[0].PR_COUNT[0];
                    }
                }
                else {
                    console.log("error found in err tag: ", err);
                }
            });
        });
    }
    doRefresh(refresher) {
        this.getPending_counts(refresher);
        // setTimeout(() => {
        //   refresher.complete();
        // }, 200);
    }
    prlist() {
        // let that = this;
        this.router.navigateByUrl('/pr-list');
    }
    polist() {
        // let that = this;
        this.router.navigateByUrl('/po-list');
    }
};
HomePage.ctorParameters = () => [
    { type: _services_hero_service__WEBPACK_IMPORTED_MODULE_2__.HeroService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.ToastController },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__.Router },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.LoadingController }
];
HomePage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-home',
        template: _raw_loader_home_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_home_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], HomePage);



/***/ }),

/***/ 2610:
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".divNew {\n  padding: auto;\n  margin: 3%;\n}\n\n.buttonClass {\n  display: inline-block;\n  background: lightgray;\n  min-height: 150px;\n  padding: auto;\n  color: #000;\n  font-weight: bold;\n  text-transform: uppercase;\n  letter-spacing: 1px;\n  border-radius: 5px;\n  border: 1px solid white;\n  box-shadow: 0px 17px 10px -10px rgba(0, 0, 0, 0.4);\n  cursor: pointer;\n  transition: all ease-in-out 300ms;\n  touch-action: pan-y;\n}\n\n.button-md,\n.button-ios {\n  height: 15.6rem;\n  font-size: 20px;\n}\n\n.buttonClass:active {\n  background-color: #0065b3;\n  transform: translateY(4px);\n}\n\n.spanSt {\n  color: #000;\n  margin-top: 15%;\n  float: right;\n  font-size: 36px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImhvbWUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsYUFBQTtFQUNBLFVBQUE7QUFDRjs7QUFFQTtFQUNFLHFCQUFBO0VBQ0EscUJBQUE7RUFFQSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSx1QkFBQTtFQUNBLGtEQUFBO0VBQ0EsZUFBQTtFQUNBLGlDQUFBO0VBQ0EsbUJBQUE7QUFBRjs7QUFHQTs7RUFJRSxlQUFBO0VBQ0EsZUFBQTtBQUZGOztBQU1BO0VBQ0UseUJBQUE7RUFDQSwwQkFBQTtBQUhGOztBQTBCQTtFQUNFLFdBQUE7RUFFQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUF4QkYiLCJmaWxlIjoiaG9tZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZGl2TmV3e1xuICBwYWRkaW5nOiBhdXRvO1xuICBtYXJnaW46IDMlO1xufVxuXG4uYnV0dG9uQ2xhc3Mge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGJhY2tncm91bmQ6IGxpZ2h0Z3JheTtcbiAgLy8gcGFkZGluZzogNTVweCA0cmVtO1xuICBtaW4taGVpZ2h0OiAxNTBweDtcbiAgcGFkZGluZzogYXV0bztcbiAgY29sb3I6ICMwMDA7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBsZXR0ZXItc3BhY2luZzogMXB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkIHdoaXRlO1xuICBib3gtc2hhZG93OiAwcHggMTdweCAxMHB4IC0xMHB4IHJnYmEoMCwgMCwgMCwgMC40KTtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICB0cmFuc2l0aW9uOiBhbGwgZWFzZS1pbi1vdXQgMzAwbXM7XG4gIHRvdWNoLWFjdGlvbjogcGFuLXk7IFxuICAvLyB0ZXh0LWFsaWduOiBjZW50ZXJcbn1cbi5idXR0b24tbWQsXG4uYnV0dG9uLWlvcyB7XG4gIC8vIGJvcmRlci1yYWRpdXM6IDJweDtcbiAgLy8gb3ZlcmZsb3c6IGhpZGRlbjtcbiAgaGVpZ2h0OiAxNS42cmVtO1xuICBmb250LXNpemU6IDIwcHg7XG4gIC8vIGZvbnQtc2l6ZTogMS40cmVtO1xufVxuXG4uYnV0dG9uQ2xhc3M6YWN0aXZlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwNjViMztcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKDRweCk7XG59XG5cbi8vICNub3RpZmljYXRpb24tYnV0dG9uIHsgICAgICAgICAgICBcbi8vICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4vLyAgICAgLy8gd2lkdGg6IDQycHg7XG4vLyAgICAgdG9wOjFweDtcbi8vICAgICByaWdodDogMXB4O1xuLy8gICAgIG92ZXJmbG93OiB2aXNpYmxlIWltcG9ydGFudDtcbi8vIH1cblxuXG4vLyAjbm90aWZpY2F0aW9ucy1iYWRnZSB7XG4vLyAgICAgLy8gYmFja2dyb3VuZC1jb2xvcjogcmVkO1xuLy8gICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbi8vICAgICAvLyB0b3A6IC0zcHg7XG4vLyAgICAgcmlnaHQ6IDNweDtcbi8vICAgICAvLyByaWdodDogNXB4O1xuLy8gICAgIC8vIGJvcmRlci1yYWRpdXM6IDEwMCU7XG4vLyAgICAgLy8gbGVmdDogMHB4O1xuLy8gICAgIGJvdHRvbTogMDtcbi8vIH1cblxuLnNwYW5TdHtcbiAgY29sb3I6ICMwMDA7XG4gIC8vIHRvcDogMjA7XG4gIG1hcmdpbi10b3A6IDE1JTtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBmb250LXNpemU6IDM2cHg7XG59Il19 */");

/***/ }),

/***/ 49764:
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html ***!
  \***************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\">\n  <ion-toolbar color=\"primary\">\n    <ion-title mode=\"ios\">\n      Ramky\n    </ion-title>\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"heroService.logout()\">\n        <ion-icon slot=\"icon-only\" name=\"log-out-outline\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding\">\n  \n    <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\n      <ion-refresher-content\n        pullingIcon=\"chevron-down-circle-outline\"\n        pullingText=\"Pull to refresh\"\n        refreshingSpinner=\"circles\"\n        refreshingText=\"Refreshing...\">\n      </ion-refresher-content>\n    </ion-refresher>\n  \n  <ion-row  style=\"margin-top: 15%;padding: 8px;\">\n    <div style=\"width: 100%; float: left; \">\n      <ion-col class=\"buttonClass\" (click)=\"prlist()\">\n        <p style=\"font-size: 18px;\">Purchase Requisitions</p>\n        <span class=\"spanSt\">{{_prCount}}</span>\n      </ion-col>\n    </div>\n    </ion-row>\n  \n    <ion-row style=\"padding: 8px; \">\n      \n      <div style=\"width:100%; float: right;\">\n        <ion-col class=\"buttonClass\" (click)=\"polist()\">\n          <p style=\"font-size: 18px;\">Purchase Orders</p>\n          <span class=\"spanSt\">{{_poCount}}</span>\n        </ion-col>\n      </div>\n    </ion-row>\n  <!-- <ion-header collapse=\"condense\">\n    <ion-toolbar>\n      <ion-title size=\"large\">Blank</ion-title>\n    </ion-toolbar>\n  </ion-header> -->\n\n  <!-- <div id=\"container\">\n    <strong>Ready to create an app?</strong>\n    <p>Start with Ionic <a target=\"_blank\" rel=\"noopener noreferrer\" href=\"https://ionicframework.com/docs/components\">UI Components</a></p>\n  </div> -->\n</ion-content>\n");

/***/ })

}]);
//# sourceMappingURL=src_app_home_home_module_ts-es2015.js.map