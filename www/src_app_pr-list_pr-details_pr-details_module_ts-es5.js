(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (self["webpackChunkRamkyPOPR"] = self["webpackChunkRamkyPOPR"] || []).push([["src_app_pr-list_pr-details_pr-details_module_ts"], {
    /***/
    16342: function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "PrDetailsPageRoutingModule": function PrDetailsPageRoutingModule() {
          return (
            /* binding */
            _PrDetailsPageRoutingModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      39895);
      /* harmony import */


      var _pr_details_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./pr-details.page */
      6525);

      var routes = [{
        path: '',
        component: _pr_details_page__WEBPACK_IMPORTED_MODULE_0__.PrDetailsPage
      }];

      var _PrDetailsPageRoutingModule = function PrDetailsPageRoutingModule() {
        _classCallCheck(this, PrDetailsPageRoutingModule);
      };

      _PrDetailsPageRoutingModule = (0, tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule]
      })], _PrDetailsPageRoutingModule);
      /***/
    },

    /***/
    99992: function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "PrDetailsPageModule": function PrDetailsPageModule() {
          return (
            /* binding */
            _PrDetailsPageModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common */
      38583);
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/forms */
      3679);
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/angular */
      80476);
      /* harmony import */


      var _pr_details_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./pr-details-routing.module */
      16342);
      /* harmony import */


      var _pr_details_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./pr-details.page */
      6525);

      var _PrDetailsPageModule = function PrDetailsPageModule() {
        _classCallCheck(this, PrDetailsPageModule);
      };

      _PrDetailsPageModule = (0, tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule, _pr_details_routing_module__WEBPACK_IMPORTED_MODULE_0__.PrDetailsPageRoutingModule],
        declarations: [_pr_details_page__WEBPACK_IMPORTED_MODULE_1__.PrDetailsPage]
      })], _PrDetailsPageModule);
      /***/
    },

    /***/
    6525: function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "PrDetailsPage": function PrDetailsPage() {
          return (
            /* binding */
            _PrDetailsPage
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _raw_loader_pr_details_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! !raw-loader!./pr-details.page.html */
      87669);
      /* harmony import */


      var _pr_details_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./pr-details.page.scss */
      46207);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @angular/router */
      39895);
      /* harmony import */


      var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic-native/http/ngx */
      68589);
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @ionic/angular */
      80476);
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! rxjs/operators */
      28049);
      /* harmony import */


      var src_app_services_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/services/data.service */
      52468);
      /* harmony import */


      var src_app_services_hero_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/app/services/hero.service */
      19405);
      /* harmony import */


      var src_app_services_urls__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/app/services/urls */
      60881);
      /* harmony import */


      var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic-native/in-app-browser/ngx */
      53760);

      var _PrDetailsPage = /*#__PURE__*/function () {
        function PrDetailsPage(dataService, heroService, loadingController, urls, alertController, router, http, iab) {
          _classCallCheck(this, PrDetailsPage);

          this.dataService = dataService;
          this.heroService = heroService;
          this.loadingController = loadingController;
          this.urls = urls;
          this.alertController = alertController;
          this.router = router;
          this.http = http;
          this.iab = iab;
          this.allData = {};
          this._lineDetails = [];
          this._headerDetails = {};
          this.showDiv = false;
          this._addedCommts = "";
        }

        _createClass(PrDetailsPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            this.dataService.currentData.pipe((0, rxjs_operators__WEBPACK_IMPORTED_MODULE_7__.first)()).subscribe(function (data) {
              var data1 = data;
              console.log("Transfered pr object: ", data1);

              if (data1 != undefined) {
                _this._get_prDetails(data1);
              }
            });
          }
        }, {
          key: "_get_prDetails",
          value: function _get_prDetails(item) {
            var that = this;
            var dataObj = {
              _name: "SOAP:Envelope",
              _attrs: {
                "xmlns:SOAP": "http://schemas.xmlsoap.org/soap/envelope/"
              },
              _content: {
                "SOAP:Body": [{
                  _name: "PreparexmlforPRpdf",
                  _attrs: {
                    "xmlns": "http://schemas.cordys.com/PRPOApprovalsWSApp"
                  },
                  _content: [{
                    "PRNumber": item.PRNUMBER,
                    "Source": "Mobile"
                  }]
                }]
              }
            };
            that.loadingController.create({
              spinner: 'bubbles',
              message: 'Please wait...'
            }).then(function (loadEl) {
              loadEl.present();
              that.heroService.testService(dataObj, function (err, response) {
                loadEl.dismiss();
                debugger;

                if (response) {
                  var s = response['SOAP:Envelope'];
                  var faultString = s['SOAP:Body'][0]['SOAP:Fault'];

                  if (faultString != undefined) {
                    console.log("fault string: ", faultString[0].faultstring[0]._); // if (faultString[0].faultstring[0]._ == "Unable to bind the artifact to a SAML assertion.") {

                    that.heroService._toastrErrorMsg(faultString[0].faultstring[0]._);

                    return; // }
                  }

                  var obj = $.cordys.json.findObjects(response, "PRPDFData");
                  console.log("check PR obj: ", obj);

                  if (obj[0] != undefined) {
                    that.allData = obj[0];
                    console.log("allData: ", that.allData);
                    that._headerDetails = that.allData.PRHeaderInfo[0];
                    that._lineDetails = that.allData.PRLinesInfo[0].Item;
                    that.PDF_URL = that._headerDetails.PDF_URL ? that._headerDetails.PDF_URL[0] : undefined;
                  }
                } else {
                  console.log("error found in err tag: ", err);
                }

                that._getStatusForSendBackBtn(item);
              });
            });
          }
        }, {
          key: "_getStatusForSendBackBtn",
          value: function _getStatusForSendBackBtn(item) {
            var that = this;
            var dataObj = {
              _name: "SOAP:Envelope",
              _attrs: {
                "xmlns:SOAP": "http://schemas.xmlsoap.org/soap/envelope/"
              },
              _content: {
                "SOAP:Body": [{
                  _name: "IsSendBackAllowedForPR",
                  _attrs: {
                    "xmlns": "http://schemas.cordys.com/PRPOApprovalsWSApp"
                  },
                  _content: [{
                    "prNumber": item.PRNUMBER
                  }]
                }]
              }
            };
            that.loadingController.create({
              spinner: 'bubbles',
              message: 'Please wait...'
            }).then(function (loadEl) {
              loadEl.present();
              that.heroService.testService(dataObj, function (err, response) {
                loadEl.dismiss();
                debugger;

                if (response) {
                  var obj = $.cordys.json.findObjects(response, "IsSendBackAllowedForPRResponse");
                  console.log("check PR send back obj: ", obj);

                  if (obj[0] != undefined) {
                    that.allData._isSendBackShow = obj[0]["return"][0];
                    console.log("allData: ", that.allData);
                  }
                } else {
                  console.log("error found in err tag: ", err);
                }
              });
            });
          }
        }, {
          key: "_downloadDoc",
          value: function _downloadDoc() {
            console.log("Download doc executed!");

            if (this.PDF_URL != undefined && this.PDF_URL != "null") {
              window.open(this.PDF_URL, '_system'); // this.iab.create(this.PDF_URL, '_self', 'location=no');
            } else {
              this.heroService._toastrErrorMsg("PDF URL not found!");
            }
          }
        }, {
          key: "_approve",
          value: function _approve() {
            var _this2 = this;

            var _url = encodeURI(this.urls._url + "home/REEL/com.reel1.callbackgateway.prpogateway.wcp?TaskID=" + this._headerDetails.TASK_ID[0] + "&Decision=Approved&Comment=" + this._addedCommts + "&BTN=&Mode=Mobile");

            console.log("Approve PR URL: ", _url);
            this.alertController.create({
              header: 'Confirm!',
              message: 'Are you sure you want to proceed?',
              buttons: [{
                text: 'Cancel',
                role: 'cancel',
                cssClass: 'secondary',
                handler: function handler(blah) {
                  console.log('Confirm Cancel: blah');
                }
              }, {
                text: 'PROCEED',
                handler: function handler() {
                  console.log('Confirm Okay');

                  _this2.loadingController.create({
                    spinner: 'bubbles',
                    message: 'Please wait...'
                  }).then(function (loadEL) {
                    loadEL.present();

                    _this2.http.get(_url, {}, {}).then(function (data) {
                      loadEL.dismiss();
                      var str = data.data.toString();
                      var $str1 = $(str);
                      var finalMsg = $str1.find('p').eq(0).text();

                      _this2.heroService._toastrMsg(finalMsg);

                      setTimeout(function () {
                        _this2.router.navigateByUrl('/home');
                      }, 3000);
                    })["catch"](function (error) {
                      loadEL.dismiss();
                      console.log(error.status);
                      console.log(error.error); // error message as string

                      console.log(error.headers);

                      _this2.heroService._toastrErrorMsg("Error occured while proccessing request. Please contact administrator.");
                    });
                  });
                }
              }]
            }).then(function (alertEl) {
              alertEl.present();
            });
          }
        }, {
          key: "_reject",
          value: function _reject() {
            var _this3 = this;

            var _url = encodeURI(this.urls._url + "home/REEL/com.reel1.callbackgateway.prpogateway.wcp?TaskID=" + this._headerDetails.TASK_ID[0] + "&Decision=Rejected&Comment=" + this._addedCommts + "&BTN=&Mode=Mobile");

            this.alertController.create({
              header: 'Confirm!',
              message: 'Are you sure you want to proceed?',
              buttons: [{
                text: 'Cancel',
                role: 'cancel',
                cssClass: 'secondary',
                handler: function handler(blah) {
                  console.log('Confirm Cancel: blah');
                }
              }, {
                text: 'PROCEED',
                handler: function handler() {
                  console.log('Confirm Okay');

                  _this3.loadingController.create({
                    spinner: 'bubbles',
                    message: 'Please wait...'
                  }).then(function (loadEL) {
                    loadEL.present();

                    _this3.http.get(_url, {}, {}).then(function (data) {
                      loadEL.dismiss();
                      var str = data.data.toString();
                      var $str1 = $(str);
                      var finalMsg = $str1.find('p').eq(0).text();

                      _this3.heroService._toastrMsg(finalMsg);

                      setTimeout(function () {
                        _this3.router.navigateByUrl('/home');
                      }, 3000);
                    })["catch"](function (error) {
                      loadEL.dismiss();
                      console.log(error.status);
                      console.log(error.error); // error message as string

                      console.log(error.headers);

                      _this3.heroService._toastrErrorMsg("Error occured while proccessing request. Please contact administrator.");
                    });
                  });
                }
              }]
            }).then(function (alertEl) {
              alertEl.present();
            });
          }
        }, {
          key: "_sendback",
          value: function _sendback() {
            var _this4 = this;

            var _url = encodeURI(this.urls._url + "home/REEL/com.reel1.callbackgateway.prpogateway.wcp?TaskID=" + this._headerDetails.TASK_ID[0] + "&Decision=SendBack&Comment=" + this._addedCommts + "&BTN=&Mode=Mobile");

            this.alertController.create({
              header: 'Confirm!',
              message: 'Are you sure you want to proceed?',
              buttons: [{
                text: 'Cancel',
                role: 'cancel',
                cssClass: 'secondary',
                handler: function handler(blah) {
                  console.log('Confirm Cancel: blah');
                }
              }, {
                text: 'PROCEED',
                handler: function handler() {
                  console.log('Confirm Okay');

                  _this4.loadingController.create({
                    spinner: 'bubbles',
                    message: 'Please wait...'
                  }).then(function (loadEL) {
                    loadEL.present();

                    _this4.http.get(_url, {}, {}).then(function (data) {
                      loadEL.dismiss();
                      var str = data.data.toString();
                      var $str1 = $(str);
                      var finalMsg = $str1.find('p').eq(0).text();

                      _this4.heroService._toastrMsg(finalMsg);

                      setTimeout(function () {
                        _this4.router.navigateByUrl('/home');
                      }, 3000);
                    })["catch"](function (error) {
                      loadEL.dismiss();
                      console.log(error.status);
                      console.log(error.error); // error message as string

                      console.log(error.headers);

                      _this4.heroService._toastrErrorMsg("Error occured while proccessing request. Please contact administrator.");
                    });
                  });
                }
              }]
            }).then(function (alertEl) {
              alertEl.present();
            });
          }
        }]);

        return PrDetailsPage;
      }();

      _PrDetailsPage.ctorParameters = function () {
        return [{
          type: src_app_services_data_service__WEBPACK_IMPORTED_MODULE_3__.DataService
        }, {
          type: src_app_services_hero_service__WEBPACK_IMPORTED_MODULE_4__.HeroService
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.LoadingController
        }, {
          type: src_app_services_urls__WEBPACK_IMPORTED_MODULE_5__.URLS
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.AlertController
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_9__.Router
        }, {
          type: _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_2__.HTTP
        }, {
          type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_6__.InAppBrowser
        }];
      };

      _PrDetailsPage = (0, tslib__WEBPACK_IMPORTED_MODULE_10__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_11__.Component)({
        selector: 'app-pr-details',
        template: _raw_loader_pr_details_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_pr_details_page_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
      })], _PrDetailsPage);
      /***/
    },

    /***/
    46207: function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".divClass {\n  position: relative;\n  font-size: 1.1em;\n  font-weight: normal;\n  text-transform: none;\n  color: #000;\n  background-color: #fff;\n  box-shadow: none;\n  transition: background-color 300ms cubic-bezier(0.4, 0, 0.2, 1);\n  border-bottom: 1px solid #dedede;\n}\n\n.label-md {\n  margin: 0px !important;\n}\n\n.inpSTy .text-input-md,\n.inpSTy .text-input-ios {\n  margin: 0px !important;\n  padding: 0;\n  width: calc(100% - 8px - 8px);\n  background-color: transparent;\n}\n\n.inst .text-input-md,\n.inst .text-input-ios {\n  margin: 5px 8px;\n  padding: 4px;\n  width: calc(100% - 8px - 8px);\n  background-color: #36658a;\n  border-radius: 2px;\n  color: white;\n}\n\nion-input {\n  background-color: #36658a;\n  border-radius: 2px;\n}\n\n.toggle-md,\n.toggle-ios {\n  position: relative;\n  width: 30px;\n  height: 10px;\n  box-sizing: content-box;\n  contain: strict;\n  padding: 7px;\n}\n\n.toggle-md .toggle-inner,\n.toggle-ios .toggle-inner {\n  top: -1px;\n  width: 15px;\n  height: 15px;\n}\n\n.toggle-md .toggle-icon,\n.toggle-ios .toggle-icon {\n  width: 30px;\n  height: 14px;\n  background-color: #dedede;\n}\n\n.radio .radio-icon {\n  display: none;\n}\n\n.cc-selector input {\n  margin: 0;\n  padding: 0;\n}\n\n.approve {\n  height: 100;\n}\n\n.drinkcard-cc {\n  cursor: pointer;\n  background-size: contain;\n  background-repeat: no-repeat;\n  display: inline-block;\n  width: 100px;\n  height: 40px;\n  transition: all 100ms ease-in;\n  -moz-filter: brightness(1.8) grayscale(1) opacity(0.7);\n  filter: brightness(1.8) grayscale(1) opacity(0.7);\n}\n\n.drinkcard-ee {\n  cursor: pointer;\n  background-size: contain;\n  background-repeat: no-repeat;\n  display: inline-block;\n  width: 100px;\n  height: 40px;\n  transition: all 100ms ease-in;\n}\n\n.approveopacity {\n  -moz-filter: brightness(1) grayscale(1) opacity(0.3);\n  filter: brightness(1) grayscale(1) opacity(0.3);\n}\n\n.rejectopacit {\n  -moz-filter: brightness(1.8) grayscale(1) opacity(0.3);\n  filter: brightness(1.8) grayscale(1) opacity(0.3);\n}\n\n.drinkcard-dd {\n  cursor: pointer;\n  background-size: contain;\n  background-repeat: no-repeat;\n  display: inline-block;\n  width: 100px;\n  height: 40px;\n  transition: all 100ms ease-in;\n  -moz-filter: brightness(1.8) grayscale(1) opacity(0.7);\n  filter: brightness(1.8) grayscale(1) opacity(0.7);\n}\n\n.drinkcard-bb {\n  cursor: pointer;\n  background-size: contain;\n  background-repeat: no-repeat;\n  display: inline-block;\n  width: 100px;\n  height: 40px;\n  transition: all 100ms ease-in;\n}\n\n.drinkcard-cc:hover {\n  -moz-filter: brightness(1.2) grayscale(0.5) opacity(0.9);\n  filter: brightness(1.2) grayscale(0.5) opacity(0.9);\n}\n\n/* Extras */\n\na:visited {\n  color: #888;\n}\n\na {\n  color: #444;\n  text-decoration: none;\n}\n\np {\n  margin-bottom: 0.3em;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByLWRldGFpbHMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0Esb0JBQUE7RUFDQSxXQUFBO0VBQ0Esc0JBQUE7RUFFQSxnQkFBQTtFQUVBLCtEQUFBO0VBQ0EsZ0NBQUE7QUFDSjs7QUFFQTtFQUNJLHNCQUFBO0FBQ0o7O0FBR0k7O0VBRUksc0JBQUE7RUFDQSxVQUFBO0VBQ0EsNkJBQUE7RUFDQSw2QkFBQTtBQUFSOztBQUlJOztFQUVJLGVBQUE7RUFDQSxZQUFBO0VBQ0EsNkJBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtBQURSOztBQUlBO0VBQ0kseUJBQUE7RUFDQSxrQkFBQTtBQURKOztBQUlBOztFQUVJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFFQSx1QkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0FBREo7O0FBR0E7O0VBRUksU0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FBQUo7O0FBR0E7O0VBRUksV0FBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtBQUFKOztBQUdBO0VBQ0ksYUFBQTtBQUFKOztBQUVBO0VBQ0ksU0FBQTtFQUNBLFVBQUE7QUFDSjs7QUFDQTtFQUVJLFdBQUE7QUFDSjs7QUFLQTtFQUNJLGVBQUE7RUFDQSx3QkFBQTtFQUNBLDRCQUFBO0VBQ0EscUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUdBLDZCQUFBO0VBRUEsc0RBQUE7RUFDQSxpREFBQTtBQUZKOztBQUtBO0VBQ0ksZUFBQTtFQUNBLHdCQUFBO0VBQ0EsNEJBQUE7RUFDQSxxQkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBRUEsNkJBQUE7QUFGSjs7QUFLQTtFQUVJLG9EQUFBO0VBQ0EsK0NBQUE7QUFGSjs7QUFJQTtFQUVJLHNEQUFBO0VBQ0EsaURBQUE7QUFESjs7QUFJQTtFQUNJLGVBQUE7RUFDQSx3QkFBQTtFQUNBLDRCQUFBO0VBQ0EscUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUdBLDZCQUFBO0VBRUEsc0RBQUE7RUFDQSxpREFBQTtBQURKOztBQUlBO0VBQ0ksZUFBQTtFQUNBLHdCQUFBO0VBQ0EsNEJBQUE7RUFDQSxxQkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBR0EsNkJBQUE7QUFESjs7QUFHQTtFQUVJLHdEQUFBO0VBQ0EsbURBQUE7QUFBSjs7QUFHQSxXQUFBOztBQUNBO0VBQ0ksV0FBQTtBQUFKOztBQUVBO0VBQ0ksV0FBQTtFQUNBLHFCQUFBO0FBQ0o7O0FBQ0E7RUFDSSxvQkFBQTtBQUVKIiwiZmlsZSI6InByLWRldGFpbHMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmRpdkNsYXNzIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgZm9udC1zaXplOiAxLjFlbTtcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xuICAgIHRleHQtdHJhbnNmb3JtOiBub25lO1xuICAgIGNvbG9yOiAjMDAwO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiBub25lO1xuICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiBiYWNrZ3JvdW5kLWNvbG9yIDMwMG1zIGN1YmljLWJlemllcigwLjQsIDAsIDAuMiwgMSk7XG4gICAgdHJhbnNpdGlvbjogYmFja2dyb3VuZC1jb2xvciAzMDBtcyBjdWJpYy1iZXppZXIoMC40LCAwLCAwLjIsIDEpO1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZGVkZWRlO1xufVxuXG4ubGFiZWwtbWQge1xuICAgIG1hcmdpbjogMHB4ICFpbXBvcnRhbnQ7XG59XG5cbi5pbnBTVHkge1xuICAgIC50ZXh0LWlucHV0LW1kLFxuICAgIC50ZXh0LWlucHV0LWlvcyB7XG4gICAgICAgIG1hcmdpbjogMHB4ICFpbXBvcnRhbnQ7XG4gICAgICAgIHBhZGRpbmc6IDA7XG4gICAgICAgIHdpZHRoOiBjYWxjKDEwMCUgLSA4cHggLSA4cHgpO1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgICB9XG59XG4uaW5zdCB7XG4gICAgLnRleHQtaW5wdXQtbWQsXG4gICAgLnRleHQtaW5wdXQtaW9zIHtcbiAgICAgICAgbWFyZ2luOiA1cHggOHB4O1xuICAgICAgICBwYWRkaW5nOiA0cHg7XG4gICAgICAgIHdpZHRoOiBjYWxjKDEwMCUgLSA4cHggLSA4cHgpO1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzY2NThhO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAycHg7XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICB9XG59XG5pb24taW5wdXQge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMzNjY1OGE7XG4gICAgYm9yZGVyLXJhZGl1czogMnB4O1xufVxuXG4udG9nZ2xlLW1kLFxuLnRvZ2dsZS1pb3Mge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB3aWR0aDogMzBweDtcbiAgICBoZWlnaHQ6IDEwcHg7XG4gICAgLXdlYmtpdC1ib3gtc2l6aW5nOiBjb250ZW50LWJveDtcbiAgICBib3gtc2l6aW5nOiBjb250ZW50LWJveDtcbiAgICBjb250YWluOiBzdHJpY3Q7XG4gICAgcGFkZGluZzogN3B4O1xufVxuLnRvZ2dsZS1tZCAudG9nZ2xlLWlubmVyLFxuLnRvZ2dsZS1pb3MgLnRvZ2dsZS1pbm5lciB7XG4gICAgdG9wOiAtMXB4O1xuICAgIHdpZHRoOiAxNXB4O1xuICAgIGhlaWdodDogMTVweDtcbn1cblxuLnRvZ2dsZS1tZCAudG9nZ2xlLWljb24sXG4udG9nZ2xlLWlvcyAudG9nZ2xlLWljb24ge1xuICAgIHdpZHRoOiAzMHB4O1xuICAgIGhlaWdodDogMTRweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZGVkZWRlO1xufVxuXG4ucmFkaW8gLnJhZGlvLWljb24ge1xuICAgIGRpc3BsYXk6IG5vbmU7XG59XG4uY2Mtc2VsZWN0b3IgaW5wdXQge1xuICAgIG1hcmdpbjogMDtcbiAgICBwYWRkaW5nOiAwO1xufVxuLmFwcHJvdmUge1xuICAgIC8vIGJhY2tncm91bmQtaW1hZ2U6IHVybCguLi9hc3NldHMvaW1ncy9hcHByb3ZlLnBuZyk7XG4gICAgaGVpZ2h0OiAxMDA7XG59XG4ucmVqZWN0IHtcbiAgICAvLyBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoLi4vYXNzZXRzL2ltZ3MvcmVqZWN0LnBuZyk7XG59XG5cbi5kcmlua2NhcmQtY2Mge1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgd2lkdGg6IDEwMHB4O1xuICAgIGhlaWdodDogNDBweDtcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAxMDBtcyBlYXNlLWluO1xuICAgIC1tb3otdHJhbnNpdGlvbjogYWxsIDEwMG1zIGVhc2UtaW47XG4gICAgdHJhbnNpdGlvbjogYWxsIDEwMG1zIGVhc2UtaW47XG4gICAgLXdlYmtpdC1maWx0ZXI6IGJyaWdodG5lc3MoMS44KSBncmF5c2NhbGUoMSkgb3BhY2l0eSgwLjcpO1xuICAgIC1tb3otZmlsdGVyOiBicmlnaHRuZXNzKDEuOCkgZ3JheXNjYWxlKDEpIG9wYWNpdHkoMC43KTtcbiAgICBmaWx0ZXI6IGJyaWdodG5lc3MoMS44KSBncmF5c2NhbGUoMSkgb3BhY2l0eSgwLjcpO1xufVxuXG4uZHJpbmtjYXJkLWVlIHtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOiAxMDBweDtcbiAgICBoZWlnaHQ6IDQwcHg7XG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMTAwbXMgZWFzZS1pbjtcbiAgICB0cmFuc2l0aW9uOiBhbGwgMTAwbXMgZWFzZS1pbjtcbn1cblxuLmFwcHJvdmVvcGFjaXR5IHtcbiAgICAtd2Via2l0LWZpbHRlcjogYnJpZ2h0bmVzcygxKSBncmF5c2NhbGUoMSkgb3BhY2l0eSgwLjMpO1xuICAgIC1tb3otZmlsdGVyOiBicmlnaHRuZXNzKDEpIGdyYXlzY2FsZSgxKSBvcGFjaXR5KDAuMyk7XG4gICAgZmlsdGVyOiBicmlnaHRuZXNzKDEpIGdyYXlzY2FsZSgxKSBvcGFjaXR5KDAuMyk7XG59XG4ucmVqZWN0b3BhY2l0IHtcbiAgICAtd2Via2l0LWZpbHRlcjogYnJpZ2h0bmVzcygxLjgpIGdyYXlzY2FsZSgxKSBvcGFjaXR5KDAuMyk7XG4gICAgLW1vei1maWx0ZXI6IGJyaWdodG5lc3MoMS44KSBncmF5c2NhbGUoMSkgb3BhY2l0eSgwLjMpO1xuICAgIGZpbHRlcjogYnJpZ2h0bmVzcygxLjgpIGdyYXlzY2FsZSgxKSBvcGFjaXR5KDAuMyk7XG59XG5cbi5kcmlua2NhcmQtZGQge1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgd2lkdGg6IDEwMHB4O1xuICAgIGhlaWdodDogNDBweDtcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAxMDBtcyBlYXNlLWluO1xuICAgIC1tb3otdHJhbnNpdGlvbjogYWxsIDEwMG1zIGVhc2UtaW47XG4gICAgdHJhbnNpdGlvbjogYWxsIDEwMG1zIGVhc2UtaW47XG4gICAgLXdlYmtpdC1maWx0ZXI6IGJyaWdodG5lc3MoMS44KSBncmF5c2NhbGUoMSkgb3BhY2l0eSgwLjcpO1xuICAgIC1tb3otZmlsdGVyOiBicmlnaHRuZXNzKDEuOCkgZ3JheXNjYWxlKDEpIG9wYWNpdHkoMC43KTtcbiAgICBmaWx0ZXI6IGJyaWdodG5lc3MoMS44KSBncmF5c2NhbGUoMSkgb3BhY2l0eSgwLjcpO1xufVxuXG4uZHJpbmtjYXJkLWJiIHtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOiAxMDBweDtcbiAgICBoZWlnaHQ6IDQwcHg7XG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMTAwbXMgZWFzZS1pbjtcbiAgICAtbW96LXRyYW5zaXRpb246IGFsbCAxMDBtcyBlYXNlLWluO1xuICAgIHRyYW5zaXRpb246IGFsbCAxMDBtcyBlYXNlLWluO1xufVxuLmRyaW5rY2FyZC1jYzpob3ZlciB7XG4gICAgLXdlYmtpdC1maWx0ZXI6IGJyaWdodG5lc3MoMS4yKSBncmF5c2NhbGUoMC41KSBvcGFjaXR5KDAuOSk7XG4gICAgLW1vei1maWx0ZXI6IGJyaWdodG5lc3MoMS4yKSBncmF5c2NhbGUoMC41KSBvcGFjaXR5KDAuOSk7XG4gICAgZmlsdGVyOiBicmlnaHRuZXNzKDEuMikgZ3JheXNjYWxlKDAuNSkgb3BhY2l0eSgwLjkpO1xufVxuXG4vKiBFeHRyYXMgKi9cbmE6dmlzaXRlZCB7XG4gICAgY29sb3I6ICM4ODg7XG59XG5hIHtcbiAgICBjb2xvcjogIzQ0NDtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG5wIHtcbiAgICBtYXJnaW4tYm90dG9tOiAwLjNlbTtcbn1cbiJdfQ== */";
      /***/
    },

    /***/
    87669: function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"pr-list\"></ion-back-button>\n    </ion-buttons>\n    <ion-title mode=\"ios\">PR Details</ion-title>\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"_downloadDoc()\" fill=\"clear\">\n        <ion-icon slot=\"icon-only\" name=\"document-outline\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding\">\n  <div class=\"divNew\"  *ngIf=\"(_headerDetails | json) != '{}'\">\n    <ion-row>\n      <ion-col size=\"4\">\n        <b>PR Number</b>\n      </ion-col>\n      <ion-col size=\"8\">{{_headerDetails.PRNo ? _headerDetails.PRNo[0] : null}}</ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"4\">\n        <b>Plant</b>\n      </ion-col>\n      <ion-col size=\"8\">{{_headerDetails.PlantNo ? _headerDetails.PlantNo[0] : null}} - {{_headerDetails.PlantName ? _headerDetails.PlantName[0] : null}}</ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"4\">\n        <b>Purchasing Group</b>\n      </ion-col>\n      <ion-col size=\"8\">{{_headerDetails.PurchasingGroupCode ? _headerDetails.PurchasingGroupCode[0] : null}} - {{_headerDetails.PurchasingGroupDesc ? _headerDetails.PurchasingGroupDesc[0] : null}}</ion-col>\n    </ion-row>\n    <!-- <ion-row>\n      <ion-col size=\"4\">\n        <b>Requester</b>\n      </ion-col>\n      <ion-col size=\"8\">Sample Requester</ion-col>\n    </ion-row> -->\n    <ion-row>\n      <ion-col size=\"4\">\n        <b>Identor Name</b>\n      </ion-col>\n      <ion-col size=\"8\">{{_headerDetails.IdentorName ? _headerDetails.IdentorName[0] : null}}</ion-col>\n    </ion-row>\n    <!-- <ion-row>\n      <ion-col size=\"4\">\n        <b>Payterms</b>\n      </ion-col>\n      <ion-col size=\"8\">{{_headerDetails.PayTerms ? _headerDetails.PayTerms[0] : null}}</ion-col>\n    </ion-row> -->\n    <!-- <ion-row>\n      <ion-col size=\"4\">\n        <b>Incoterm</b>\n      </ion-col>\n      <ion-col size=\"8\">IncoTerm1&nbsp;IncoTerm2</ion-col>\n    </ion-row> -->\n    <!-- <ion-row>\n      <ion-col size=\"4\">\n        <b>Value</b>\n      </ion-col>\n      <ion-col size=\"8\">{{_headerDetails.Currency ? _headerDetails.Currency[0] : INR}}&nbsp;{{_headerDetails.TotalValue ? (_headerDetails.TotalValue[0] == \"null\" ? null : _headerDetails.GrandTotal[0]) : 0}}</ion-col>\n    </ion-row> -->\n    <ion-row>\n      <ion-col size=\"4\">\n        <b>Created On</b>\n      </ion-col>\n      <ion-col size=\"8\">{{_headerDetails.PRDate ? _headerDetails.PRDate[0] : null}}</ion-col>\n    </ion-row>\n\n    <!-- <ion-fab top right *ngIf=\"docsList.length > 0\">\n      <button ion-fab mini color=\"gpsc\" (click)=\"openFileList($event, objhead)\">\n        <ion-icon name=\"document\"></ion-icon>\n      </button>\n    </ion-fab>-->\n  </div>\n  <ion-list>\n    <ion-item style=\"border-bottom: 1px solid #0065b3;\">\n        <h2 slot=\"start\">Item(s)</h2>\n          <ion-button slot=\"end\" (click)=\"(showDiv = !showDiv)\" fill=\"clear\">\n            <ion-icon *ngIf=\"!showDiv\" color=\"primary\" name=\"add\" slot=\"icon-only\" style=\"font-size: 2em;\"></ion-icon>\n            <ion-icon *ngIf=\"showDiv\" color=\"danger\" name=\"remove\" slot=\"icon-only\" style=\"font-size: 2em;\"></ion-icon>\n      </ion-button>\n    </ion-item>\n    <div *ngIf=\"!showDiv\">\n      <ng-container *ngIf=\"_lineDetails.length > 0\">\n        <ion-item *ngFor=\"let item of _lineDetails\">\n          <ion-row style=\"width: 100%;\">\n            <ion-col size=\"6\" class=\"ion-no-padding\">\n              <h4>\n                <b>Item # {{item.ItemCode[0]}}</b>\n              </h4>\n            </ion-col>\n            <ion-col size=\"6\" class=\"ion-no-padding\"></ion-col>\n            <ion-col size=\"12\" class=\"ion-no-padding\">\n              <h5 class=\"ion-text ion-text-wrap\">{{item.ItemDesc[0]}}</h5>\n            </ion-col>\n            <ion-col size=\"4\" class=\"ion-no-padding\">\n              <h5>{{item.ReqdQty[0]}}&nbsp;{{item.UOM[0]}}</h5>\n            </ion-col>\n            <ion-col size=\"8\" class=\"ion-no-padding ion-text-right\">\n              <h5>{{item.Currency ? item.Currency[0] : 'INR'}}&nbsp;{{item.TotalValue ? item.TotalValue[0] : 0}}</h5>\n            </ion-col>\n          </ion-row>\n        </ion-item>\n      </ng-container>\n    </div>\n  </ion-list>\n  <!-- <div class=\"divNew\">\n    <ion-list *ngIf=\"_lineDetails.length > 0\">\n      <div *ngFor=\"let item of _lineDetails; let i = index;\" class=\"divClass\">\n        <ion-row>\n          <ion-col size=\"6\">\n            <b>Item # {{item.ItemCode[0]}}</b>\n          </ion-col>\n          <ion-col size=\"6\" style=\"text-align: right\">\n            <b>{{item.ReqdDeliDate[0]}}</b>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col size=\"12\" ion-text text-wrap>\n            {{item.ItemDesc[0]}}\n          </ion-col>\n        </ion-row>\n\n        <ion-row>\n          <ion-col  size=\"4\">\n            {{item.ReqdQty[0]}}&nbsp;{{item.UOM[0]}}\n          </ion-col>\n\n          <ion-col  size=\"8\" class=\"ion-text-right\">\n            INR&nbsp;\n            <span style=\"font-size: 18px;\">{{item.UnitPrice[0]}}</span>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col  size=\"12\">\n            Requisitioner:&nbsp;\n            <span>{{item.old[0].PurchaseRequisitionRelease[0].PRR_CREATEDBY}}</span>\n          </ion-col>\n        </ion-row>\n        <ion-row *ngIf=\"item.old[0].PurchaseRequisitionRelease[0].PRR_STATUS == '3R'\">\n          <ion-col  size=\"12\">\n            <b>Comments</b>:&nbsp;{{item.old[0].PurchaseRequisitionRelease[0].PRR_COMMEMTS}}\n          </ion-col>\n        </ion-row>\n        <ion-row *ngIf=\"item.values\">\n          <ion-col  size=\"12\">\n            <b>Comments</b>:&nbsp;{{item.values}}\n          </ion-col>\n        </ion-row>\n\n        <ion-row style=\"padding-left: 0px !important;\"\n          *ngIf=\"item.old[0].PurchaseRequisitionRelease[0].PRR_STATUS == '3A' || item.old[0].PurchaseRequisitionRelease[0].PRR_STATUS == '4'\">\n          <ion-col  size=\"4\">\n            <button ion-button small round color=\"secondary\" [disabled]=\"true\">Approve</button>\n          </ion-col>\n          <ion-col  size=\"2\">\n            <img src=\"assets/imgs/right-image-png-4.png\" height=\"20\" width=\"20\"\n              style=\"opacity: 0.5; margin-left: 4px; margin-top: 8px;\" />\n          </ion-col>\n          <ion-col  size=\"6\" style=\"text-align: right\">\n            <button ion-button small round color=\"grey\" [disabled]=\"true\">Reject</button>\n          </ion-col>\n        </ion-row>\n        <ion-row style=\"padding-left: 0px !important;\"\n          *ngIf=\"item.old[0].PurchaseRequisitionRelease[0].PRR_STATUS == '3R'\">\n          <ion-col  size=\"6\">\n            <button ion-button small round color=\"grey\" [disabled]=\"true\">Approve</button>\n          </ion-col>\n          <ion-col  size=\"2\" style=\"text-align: right\">\n            <img src=\"assets/imgs/New Project.png\" height=\"20\" width=\"20\"\n              style=\"opacity: 0.5; margin-top: 8px; margin-right: -12px\" />\n          </ion-col>\n          <ion-col  size=\"4\" style=\"text-align: right\">\n            <button ion-button small round color=\"danger\" [disabled]=\"true\">Reject</button>\n          </ion-col>\n        </ion-row>\n\n        <div *ngIf=\"item.tax != 'reject' && item.tax != 'approve'\">\n          <ion-row style=\"padding-left: 0px !important;\" *ngIf=\"item.old[0].PurchaseRequisitionRelease[0].PRR_STATUS == 1\">\n            <ion-col  size=\"6\">\n              <button ion-button small round color=\"secondary\" (tap)=\"approveFunc(p);\"\n                [ngStyle]=\"{'background-color': item.approvebuttonColor}\">Approve</button>\n            </ion-col>\n            <ion-col  size=\"6\" style=\"text-align: right\">\n              <button ion-button small round color=\"danger\" (tap)=\"rejectFunc(p);\"\n                [ngStyle]=\"{'background-color': item.rejectbuttonColor}\">Reject</button>\n            </ion-col>\n          </ion-row>\n        </div>\n\n        <div *ngIf=\"item.tax == 'reject' || item.tax == 'approve'\">\n          <ion-row style=\"padding-left: 0px !important;\" *ngIf=\"item.old[0].PurchaseRequisitionRelease[0].PRR_STATUS == 1\">\n            <ion-col  size=\"4\" *ngIf=\"item.tax == 'approve'\">\n              <button ion-button small round color=\"secondary\" (tap)=\"approveFunc(p);\"\n                [ngStyle]=\"{'background-color': item.approvebuttonColor}\">Approve</button>\n            </ion-col>\n            <ion-col  size=\"6\"*ngIf=\"item.tax == 'reject'\">\n              <button ion-button small round color=\"secondary\" (tap)=\"approveFunc(p);\"\n                [ngStyle]=\"{'background-color': item.approvebuttonColor}\">Approve</button>\n            </ion-col>\n            <ion-col  size=\"2\" *ngIf=\"item.tax == 'approve'\">\n              <img src=\"assets/imgs/right-image-png-4.png\" height=\"20\" width=\"20\"\n                style=\"margin-left: 4px; margin-top: 8px;\" />\n            </ion-col>\n\n            <ion-col  size=\"2\" style=\"text-align: right\" *ngIf=\"item.tax == 'reject'\">\n              <img src=\"assets/imgs/New Project.png\" height=\"20\" width=\"20\"\n                style=\"margin-right: -12px; margin-top: 8px;\" />\n            </ion-col>\n\n            <ion-col  size=\"6\" style=\"text-align: right\" *ngIf=\"item.tax == 'approve'\">\n              <button ion-button small round color=\"danger\" (tap)=\"rejectFunc(p);\" *ngIf=\"item.tax == 'approve'\"\n                [ngStyle]=\"{'background-color': item.rejectbuttonColor}\">Reject</button>\n              <button ion-button small round color=\"danger\" (tap)=\"rejectFunc(p);\" *ngIf=\"item.tax != 'approve'\"\n                [ngStyle]=\"{'background-color': item.rejectbuttonColor}\">Reject</button>\n            </ion-col>\n            <ion-col  size=\"4\" style=\"text-align: right\" *ngIf=\"item.tax == 'reject'\">\n              <button ion-button small round color=\"danger\" (tap)=\"rejectFunc(p);\" *ngIf=\"item.tax == 'approve'\"\n                [ngStyle]=\"{'background-color': item.rejectbuttonColor}\">Reject</button>\n              <button ion-button small round color=\"danger\" (tap)=\"rejectFunc(p);\" *ngIf=\"item.tax != 'approve'\"\n                [ngStyle]=\"{'background-color': item.rejectbuttonColor}\">Reject</button>\n            </ion-col>\n\n          </ion-row>\n        </div>\n\n        <ion-row *ngIf=\"item.showBTN\">\n          <ion-col  size=\"10\" style=\"text-align: left\">\n            <ion-input class=\"inst\" [(ngModel)]=\"item.comment\" type=\"text\" placeholder=\"add comments\"></ion-input>\n          </ion-col>\n          <ion-col  size=\"2\" style=\"text-align: right\">\n            <button ion-button small [disabled]=\"item.comment == undefined || item.comment == ''\"\n              (tap)=\"addComment(item.comment, p, i)\">Add</button>\n          </ion-col>\n        </ion-row>\n\n      </div>\n    </ion-list>\n  </div> -->\n</ion-content>\n<ion-footer class=\"ion-no-border\" mode=\"ios\">\n  <ion-toolbar>\n    <ion-toolbar>\n      <ion-item>\n        <ion-label position=\"stacked\">Add Comments</ion-label>\n        <ion-textarea rows=\"2\" placeholder=\"Enter any comments here...\" [(ngModel)]=\"_addedCommts\" style=\"border: 1px solid #0065b3;\n        border-radius: 5px;\n        padding-left: 5px;\n        padding-right: 5px;\"></ion-textarea>\n      </ion-item>\n    </ion-toolbar>\n    <ng-container *ngIf=\"allData._isSendBackShow != undefined\">\n      <ion-row *ngIf=\"allData._isSendBackShow == 'false'\">\n        <ion-col size=\"6\" class=\"ion-text-center\">\n          <ion-button expand=\"block\" color=\"primary\" (click)=\"_approve()\">Approve</ion-button>\n        </ion-col>\n        <ion-col size=\"6\" class=\"ion-text-center\">\n          <ion-button expand=\"block\" color=\"primary\" (click)=\"_reject()\">Reject</ion-button>\n        </ion-col>\n      </ion-row>\n    </ng-container>\n    <ng-container *ngIf=\"allData._isSendBackShow != undefined\">\n      <ion-row *ngIf=\"allData._isSendBackShow == 'true'\">\n        <ion-col size=\"4\" class=\"ion-text-center\">\n          <ion-button expand=\"block\" color=\"primary\" (click)=\"_approve()\">Approve</ion-button>\n        </ion-col>\n        <ion-col size=\"4\" class=\"ion-text-center\">\n          <ion-button expand=\"block\" color=\"primary\" (click)=\"_reject()\">Reject</ion-button>\n        </ion-col>\n        <ion-col size=\"4\" class=\"ion-text-center\">\n          <ion-button expand=\"block\" color=\"primary\" (click)=\"_sendback()\">Send Back</ion-button>\n        </ion-col>\n      </ion-row>\n    </ng-container>\n  </ion-toolbar>\n</ion-footer>";
      /***/
    }
  }]);
})();
//# sourceMappingURL=src_app_pr-list_pr-details_pr-details_module_ts-es5.js.map