(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (self["webpackChunkRamkyPOPR"] = self["webpackChunkRamkyPOPR"] || []).push([["src_app_po-list_po-details_po-details_module_ts"], {
    /***/
    79650: function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "PoDetailsPageRoutingModule": function PoDetailsPageRoutingModule() {
          return (
            /* binding */
            _PoDetailsPageRoutingModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      39895);
      /* harmony import */


      var _po_details_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./po-details.page */
      30340);

      var routes = [{
        path: '',
        component: _po_details_page__WEBPACK_IMPORTED_MODULE_0__.PoDetailsPage
      }];

      var _PoDetailsPageRoutingModule = function PoDetailsPageRoutingModule() {
        _classCallCheck(this, PoDetailsPageRoutingModule);
      };

      _PoDetailsPageRoutingModule = (0, tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule]
      })], _PoDetailsPageRoutingModule);
      /***/
    },

    /***/
    94308: function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "PoDetailsPageModule": function PoDetailsPageModule() {
          return (
            /* binding */
            _PoDetailsPageModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common */
      38583);
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/forms */
      3679);
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/angular */
      80476);
      /* harmony import */


      var _po_details_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./po-details-routing.module */
      79650);
      /* harmony import */


      var _po_details_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./po-details.page */
      30340);

      var _PoDetailsPageModule = function PoDetailsPageModule() {
        _classCallCheck(this, PoDetailsPageModule);
      };

      _PoDetailsPageModule = (0, tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule, _po_details_routing_module__WEBPACK_IMPORTED_MODULE_0__.PoDetailsPageRoutingModule],
        declarations: [_po_details_page__WEBPACK_IMPORTED_MODULE_1__.PoDetailsPage]
      })], _PoDetailsPageModule);
      /***/
    },

    /***/
    30340: function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "PoDetailsPage": function PoDetailsPage() {
          return (
            /* binding */
            _PoDetailsPage
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _raw_loader_po_details_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! !raw-loader!./po-details.page.html */
      87899);
      /* harmony import */


      var _po_details_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./po-details.page.scss */
      34781);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @ionic/angular */
      80476);
      /* harmony import */


      var src_app_services_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! src/app/services/data.service */
      52468);
      /* harmony import */


      var src_app_services_hero_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/services/hero.service */
      19405);
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! rxjs/operators */
      28049);
      /* harmony import */


      var src_app_services_urls__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/app/services/urls */
      60881);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @angular/router */
      39895);
      /* harmony import */


      var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic-native/http/ngx */
      68589);
      /* harmony import */


      var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic-native/in-app-browser/ngx */
      53760);
      /* harmony import */


      var _ionic_native_document_viewer_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ionic-native/document-viewer/ngx */
      11663); // import { File } from '@ionic-native/file';


      var _PoDetailsPage = /*#__PURE__*/function () {
        function PoDetailsPage(dataService, heroService, loadingController, urls, alertController, router, http, document, iab) {
          _classCallCheck(this, PoDetailsPage);

          this.dataService = dataService;
          this.heroService = heroService;
          this.loadingController = loadingController;
          this.urls = urls;
          this.alertController = alertController;
          this.router = router;
          this.http = http;
          this.document = document;
          this.iab = iab;
          this.docsList = [];
          this.showDiv = false;
          this.allData = {};
          this._headerDetails = {};
          this._lineDetails = [];
          this._addedCommts = "";
        }

        _createClass(PoDetailsPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            this.dataService.currentData.pipe((0, rxjs_operators__WEBPACK_IMPORTED_MODULE_8__.first)()).subscribe(function (data) {
              var data1 = data;
              console.log("Transfered po object: ", data1);

              if (data1 != undefined) {
                _this._get_poDetails(data1);
              }
            });
          }
        }, {
          key: "_get_poDetails",
          value: function _get_poDetails(item) {
            var that = this;
            var dataObj = {
              _name: "SOAP:Envelope",
              _attrs: {
                "xmlns:SOAP": "http://schemas.xmlsoap.org/soap/envelope/"
              },
              _content: {
                "SOAP:Body": [{
                  _name: "PreparexmlforPOpdf",
                  _attrs: {
                    "xmlns": "http://schemas.cordys.com/PRPOApprovalsWSApp"
                  },
                  _content: [{
                    "PONo": item.PONUMBER,
                    "Source": "Mobile"
                  }]
                }]
              }
            };
            that.loadingController.create({
              spinner: 'bubbles',
              message: 'Please wait...'
            }).then(function (loadEl) {
              loadEl.present();
              that.heroService.testService(dataObj, function (err, response) {
                loadEl.dismiss();
                debugger;

                if (response) {
                  var obj = $.cordys.json.findObjects(response, "POPDFData");
                  console.log("check PO obj: ", obj);

                  if (obj[0] != undefined) {
                    that.allData = obj[0];
                    console.log("allData: ", that.allData);
                    that._headerDetails = that.allData.POHeaderInfo[0];
                    that._lineDetails = that.allData.POLinesInfo[0].Item;
                    that.PDF_URL = that._headerDetails.PDF_URL ? that._headerDetails.PDF_URL[0] : undefined; // that._itemsFullyLoaded = true;
                  }
                } else {
                  console.log("error found in err tag: ", err);
                }

                that._getStatusForSendBackBtn(item);
              });
            });
          }
        }, {
          key: "_getStatusForSendBackBtn",
          value: function _getStatusForSendBackBtn(item) {
            var that = this;
            var dataObj = {
              _name: "SOAP:Envelope",
              _attrs: {
                "xmlns:SOAP": "http://schemas.xmlsoap.org/soap/envelope/"
              },
              _content: {
                "SOAP:Body": [{
                  _name: "IsSendBackAllowedForPO",
                  _attrs: {
                    "xmlns": "http://schemas.cordys.com/PRPOApprovalsWSApp"
                  },
                  _content: [{
                    "poNumber": item.PONUMBER
                  }]
                }]
              }
            };
            that.loadingController.create({
              spinner: 'bubbles',
              message: 'Please wait...'
            }).then(function (loadEl) {
              loadEl.present();
              that.heroService.testService(dataObj, function (err, response) {
                loadEl.dismiss();
                debugger;

                if (response) {
                  var obj = $.cordys.json.findObjects(response, "isSendBackAllowedForPO");
                  console.log("check PO send back obj: ", obj);

                  if (obj[0] != undefined) {
                    that.allData._isSendBackShow = obj[0].isSendBackAllowedForPO[0];
                    console.log("allData: ", that.allData);
                  }
                } else {
                  console.log("error found in err tag: ", err);
                }
              });
            });
          }
        }, {
          key: "_downloadDoc",
          value: function _downloadDoc() {
            console.log("Download doc executed! ", this.PDF_URL);

            if (this.PDF_URL != undefined && this.PDF_URL != "null") {
              window.open(this.PDF_URL, '_system'); // this.iab.create(this.PDF_URL, '_self', 'location=no');
            } else {
              this.heroService._toastrErrorMsg("PDF URL not found!");
            }
          }
        }, {
          key: "_approve",
          value: function _approve() {
            var _this2 = this;

            var _url = encodeURI(this.urls._url + "home/REEL/com.reel1.callbackgateway.prpogateway.wcp?TaskID=" + this._headerDetails.TASK_ID[0] + "&Decision=Approved&Comment=" + this._addedCommts + "&BTN=&Mode=Mobile");

            this.alertController.create({
              header: 'Confirm!',
              message: 'Are you sure you want to proceed?',
              buttons: [{
                text: 'Cancel',
                role: 'cancel',
                cssClass: 'secondary',
                handler: function handler(blah) {
                  console.log('Confirm Cancel: blah');
                }
              }, {
                text: 'PROCEED',
                handler: function handler() {
                  console.log('Confirm Okay');

                  _this2.loadingController.create({
                    spinner: 'bubbles',
                    message: 'Please wait...'
                  }).then(function (loadEL) {
                    loadEL.present();

                    _this2.http.get(_url, {}, {}).then(function (data) {
                      loadEL.dismiss();
                      var str = data.data.toString();
                      var $str1 = $(str);
                      var finalMsg = $str1.find('p').eq(0).text();

                      _this2.heroService._toastrMsg(finalMsg);

                      setTimeout(function () {
                        _this2.router.navigateByUrl('/home');
                      }, 3000);
                    })["catch"](function (error) {
                      loadEL.dismiss();
                      console.log(error.status);
                      console.log(error.error); // error message as string

                      console.log(error.headers);

                      _this2.heroService._toastrErrorMsg("Error occured while proccessing request. Please contact administrator.");
                    });
                  });
                }
              }]
            }).then(function (alertEl) {
              alertEl.present();
            });
          }
        }, {
          key: "_reject",
          value: function _reject() {
            var _this3 = this;

            var _url = encodeURI(this.urls._url + "home/REEL/com.reel1.callbackgateway.prpogateway.wcp?TaskID=" + this._headerDetails.TASK_ID[0] + "&Decision=Rejected&Comment=" + this._addedCommts + "&BTN=&Mode=Mobile");

            this.alertController.create({
              header: 'Confirm!',
              message: 'Are you sure you want to proceed?',
              buttons: [{
                text: 'Cancel',
                role: 'cancel',
                cssClass: 'secondary',
                handler: function handler(blah) {
                  console.log('Confirm Cancel: blah');
                }
              }, {
                text: 'PROCEED',
                handler: function handler() {
                  console.log('Confirm Okay');

                  _this3.loadingController.create({
                    spinner: 'bubbles',
                    message: 'Please wait...'
                  }).then(function (loadEL) {
                    loadEL.present();

                    _this3.http.get(_url, {}, {}).then(function (data) {
                      loadEL.dismiss();
                      var str = data.data.toString();
                      var $str1 = $(str);
                      var finalMsg = $str1.find('p').eq(0).text();

                      _this3.heroService._toastrMsg(finalMsg);

                      setTimeout(function () {
                        _this3.router.navigateByUrl('/home');
                      }, 3000);
                    })["catch"](function (error) {
                      loadEL.dismiss();
                      console.log(error.status);
                      console.log(error.error); // error message as string

                      console.log(error.headers);

                      _this3.heroService._toastrErrorMsg("Error occured while proccessing request. Please contact administrator.");
                    });
                  });
                }
              }]
            }).then(function (alertEl) {
              alertEl.present();
            });
          }
        }, {
          key: "_sendback",
          value: function _sendback() {
            var _this4 = this;

            var _url = encodeURI(this.urls._url + "home/REEL/com.reel1.callbackgateway.prpogateway.wcp?TaskID=" + this._headerDetails.TASK_ID[0] + "&Decision=SendBack&Comment=" + this._addedCommts + "&BTN=&Mode=Mobile");

            this.alertController.create({
              header: 'Confirm!',
              message: 'Are you sure you want to proceed?',
              buttons: [{
                text: 'Cancel',
                role: 'cancel',
                cssClass: 'secondary',
                handler: function handler(blah) {
                  console.log('Confirm Cancel: blah');
                }
              }, {
                text: 'PROCEED',
                handler: function handler() {
                  console.log('Confirm Okay');

                  _this4.loadingController.create({
                    spinner: 'bubbles',
                    message: 'Please wait...'
                  }).then(function (loadEL) {
                    loadEL.present();

                    _this4.http.get(_url, {}, {}).then(function (data) {
                      loadEL.dismiss();
                      var str = data.data.toString();
                      var $str1 = $(str);
                      var finalMsg = $str1.find('p').eq(0).text();

                      _this4.heroService._toastrMsg(finalMsg);

                      setTimeout(function () {
                        _this4.router.navigateByUrl('/home');
                      }, 3000);
                    })["catch"](function (error) {
                      loadEL.dismiss();
                      console.log(error.status);
                      console.log(error.error); // error message as string

                      console.log(error.headers);

                      _this4.heroService._toastrErrorMsg("Error occured while proccessing request. Please contact administrator.");
                    });
                  });
                }
              }]
            }).then(function (alertEl) {
              alertEl.present();
            });
          }
        }]);

        return PoDetailsPage;
      }();

      _PoDetailsPage.ctorParameters = function () {
        return [{
          type: src_app_services_data_service__WEBPACK_IMPORTED_MODULE_2__.DataService
        }, {
          type: src_app_services_hero_service__WEBPACK_IMPORTED_MODULE_3__.HeroService
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__.LoadingController
        }, {
          type: src_app_services_urls__WEBPACK_IMPORTED_MODULE_4__.URLS
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__.AlertController
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_10__.Router
        }, {
          type: _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_5__.HTTP
        }, {
          type: _ionic_native_document_viewer_ngx__WEBPACK_IMPORTED_MODULE_7__.DocumentViewer
        }, {
          type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_6__.InAppBrowser
        }];
      };

      _PoDetailsPage = (0, tslib__WEBPACK_IMPORTED_MODULE_11__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_12__.Component)({
        selector: 'app-po-details',
        template: _raw_loader_po_details_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_po_details_page_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
      })], _PoDetailsPage);
      /***/
    },

    /***/
    34781: function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".divNew {\n  padding: 5px;\n}\n\nion-row {\n  padding-top: 5px;\n  padding-bottom: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBvLWRldGFpbHMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtBQUNKOztBQUVFO0VBQ0UsZ0JBQUE7RUFDQSxtQkFBQTtBQUNKIiwiZmlsZSI6InBvLWRldGFpbHMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmRpdk5ld3tcbiAgICBwYWRkaW5nOiA1cHg7XG4gIH1cblxuICBpb24tcm93e1xuICAgIHBhZGRpbmctdG9wOiA1cHg7XG4gICAgcGFkZGluZy1ib3R0b206IDVweDtcbiAgfSJdfQ== */";
      /***/
    },

    /***/
    87899: function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"po-list\"></ion-back-button>\n    </ion-buttons>\n    <ion-title mode=\"ios\">PO Details</ion-title>\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"_downloadDoc()\" fill=\"clear\">\n        <ion-icon slot=\"icon-only\" name=\"document-outline\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding\">\n  <!-- <div class=\"divNew\">\n    <ion-row>\n      <ion-col size=\"4\">\n        <b>Plant</b>\n      </ion-col>\n      <ion-col size=\"8\">agsduagduasd</ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"4\">\n        <b>Plant Name</b>\n      </ion-col>\n      <ion-col size=\"8\">Sample Plant</ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"4\">\n        <b>Requester</b>\n      </ion-col>\n      <ion-col size=\"8\">Sample Requester</ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"4\">\n        <b>Partner Name</b>\n      </ion-col>\n      <ion-col size=\"8\">Tester One</ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"4\">\n        <b>Payterms</b>\n      </ion-col>\n      <ion-col size=\"8\">ksnd kndlksnd ansd</ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"4\">\n        <b>Incoterm</b>\n      </ion-col>\n      <ion-col size=\"8\">IncoTerm1&nbsp;IncoTerm2</ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"4\">\n        <b>Value</b>\n      </ion-col>\n      <ion-col size=\"8\">INR&nbsp;28346873</ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"4\">\n        <b>Created On</b>\n      </ion-col>\n      <ion-col size=\"8\">08-09-2021</ion-col>\n    </ion-row>\n  \n     <ion-fab top right *ngIf=\"docsList.length > 0\">\n      <button ion-fab mini color=\"gpsc\" (click)=\"openFileList($event, objhead)\">\n        <ion-icon name=\"document\"></ion-icon>\n      </button>\n    </ion-fab> \n  </div>-->\n  <div class=\"divNew\"  *ngIf=\"(_headerDetails | json) != '{}'\">\n    <ion-row>\n      <ion-col size=\"4\">\n        <b>Plant</b>\n      </ion-col>\n      <ion-col size=\"8\">{{_headerDetails.PlantNo ? _headerDetails.PlantNo[0] : null}}</ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"4\">\n        <b>Plant Name</b>\n      </ion-col>\n      <ion-col size=\"8\">{{_headerDetails.PLANT_NAME ? _headerDetails.PLANT_NAME[0] : null}}</ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"4\">\n        <b>Requester</b>\n      </ion-col>\n      <ion-col size=\"8\">{{_headerDetails.Requester ? _headerDetails.Requester[0] : null}}</ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"4\">\n        <b>Partner Name</b>\n      </ion-col>\n      <ion-col size=\"8\">{{_headerDetails.VendoName ? _headerDetails.VendoName[0] : null}}</ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"4\">\n        <b>Payterms</b>\n      </ion-col>\n      <ion-col size=\"8\">{{_headerDetails.PayTerms ? _headerDetails.PayTerms[0] : null}}</ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"4\">\n        <b>Value</b>\n      </ion-col>\n      <ion-col size=\"8\">{{_headerDetails.Currency ? _headerDetails.Currency[0] : 'INR'}}&nbsp;{{_headerDetails.GrandTotal ? (_headerDetails.GrandTotal[0] == \"null\" ? 0 : _headerDetails.GrandTotal[0]) : 0}}</ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"4\">\n        <b>Created On</b>\n      </ion-col>\n      <ion-col size=\"8\">{{_headerDetails.RequestedOn ? _headerDetails.RequestedOn[0] : null}}</ion-col>\n    </ion-row>\n\n    <!-- <ion-fab top right *ngIf=\"docsList.length > 0\">\n      <button ion-fab mini color=\"gpsc\" (click)=\"openFileList($event, objhead)\">\n        <ion-icon name=\"document\"></ion-icon>\n      </button>\n    </ion-fab>-->\n  </div>\n  <ion-list>\n    <ion-item style=\"border-bottom: 1px solid #0065b3;\">\n        <h2 slot=\"start\">Item(s)</h2>\n          <ion-button slot=\"end\" (click)=\"(showDiv = !showDiv)\" fill=\"clear\">\n            <ion-icon *ngIf=\"!showDiv\" color=\"primary\" name=\"add\" slot=\"icon-only\" style=\"font-size: 2em;\"></ion-icon>\n            <ion-icon *ngIf=\"showDiv\" color=\"danger\" name=\"remove\" slot=\"icon-only\" style=\"font-size: 2em;\"></ion-icon>\n      </ion-button>\n    </ion-item>\n    <div *ngIf=\"!showDiv\">\n      <ng-container *ngIf=\"_lineDetails.length > 0\">\n        <ion-item *ngFor=\"let item of _lineDetails\">\n          <ion-row style=\"width: 100%;\">\n            <ion-col size=\"6\" class=\"ion-no-padding\">\n              <h4>\n                <b>Item # {{item.SNo[0]}}</b>\n              </h4>\n            </ion-col>\n            <ion-col size=\"6\" class=\"ion-no-padding\"></ion-col>\n            <ion-col size=\"12\" class=\"ion-no-padding\">\n              <h5 class=\"ion-text ion-text-wrap\">{{item.ItemDesc[0]}}</h5>\n            </ion-col>\n            <ion-col size=\"4\" class=\"ion-no-padding\">\n              <h5>{{item.Quantity[0]}}&nbsp;{{item.UOM[0]}}</h5>\n            </ion-col>\n            <ion-col size=\"8\" class=\"ion-no-padding ion-text-right\">\n              <h5>{{_headerDetails.Currency ? _headerDetails.Currency[0] : 'INR'}}&nbsp;{{item.NetValue[0] ? item.NetValue[0] : 0}}</h5>\n            </ion-col>\n          </ion-row>\n        </ion-item>\n      </ng-container>\n    </div>\n  </ion-list>\n</ion-content>\n<ion-footer class=\"ion-no-border\" mode=\"ios\">\n  <ion-toolbar>\n    <ion-item>\n      <ion-label position=\"stacked\">Add Comments</ion-label>\n      <ion-textarea rows=\"2\" placeholder=\"Enter any comments here...\" [(ngModel)]=\"_addedCommts\" style=\"border: 1px solid #0065b3;\n      border-radius: 5px;\n      padding-left: 5px;\n      padding-right: 5px;\"></ion-textarea>\n    </ion-item>\n  </ion-toolbar>\n  <ion-toolbar>\n    <ng-container *ngIf=\"allData._isSendBackShow != undefined\">\n      <ion-row *ngIf=\"allData._isSendBackShow == 'false'\">\n        <ion-col size=\"6\" class=\"ion-text-center\">\n          <ion-button expand=\"block\" color=\"primary\" (click)=\"_approve()\">Approve</ion-button>\n        </ion-col>\n        <ion-col size=\"6\" class=\"ion-text-center\">\n          <ion-button expand=\"block\" color=\"primary\" (click)=\"_reject()\">Reject</ion-button>\n        </ion-col>\n      </ion-row>\n    </ng-container>\n    <ng-container *ngIf=\"allData._isSendBackShow != undefined\">\n      <ion-row *ngIf=\"allData._isSendBackShow == 'true'\">\n        <ion-col size=\"4\" class=\"ion-text-center\">\n          <ion-button expand=\"block\" color=\"primary\" (click)=\"_approve()\">Approve</ion-button>\n        </ion-col>\n        <ion-col size=\"4\" class=\"ion-text-center\">\n          <ion-button expand=\"block\" color=\"primary\" (click)=\"_reject()\">Reject</ion-button>\n        </ion-col>\n        <ion-col size=\"4\" class=\"ion-text-center\">\n          <ion-button expand=\"block\" color=\"primary\" (click)=\"_sendback()\">Send Back</ion-button>\n        </ion-col>\n      </ion-row>\n    </ng-container>\n  </ion-toolbar>\n</ion-footer>";
      /***/
    }
  }]);
})();
//# sourceMappingURL=src_app_po-list_po-details_po-details_module_ts-es5.js.map