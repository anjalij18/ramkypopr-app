import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { HeroService } from '../services/hero.service';
declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  _poCount: any = 0;
  _prCount: any = 0;

  constructor(
    public heroService: HeroService,
    private toastController: ToastController,
    private router: Router,
    private loadingController: LoadingController
  ) { }

  ngOnInit() {
    // this.getPending_counts();
  }

  ionViewDidEnter() {
    this.getPending_counts();
  }

  getPending_counts(refresher?: any) {
    let that = this;
    let user;
    if (localStorage.getItem("SAPIDS") != null) {
      user = localStorage.getItem("SAPIDS");
    } else {
      user = null;
    }

    console.log("check SAP IDs: ", user);
    let dataObj = {
      _name: "SOAP:Envelope",
      _attrs: {
        "xmlns:SOAP": "http://schemas.xmlsoap.org/soap/envelope/"
      },
      _content: {
        "SOAP:Body": [
          {
            _name: "GetPendingCounts",
            _attrs: {
              "xmlns": "http://schemas.cordys.com/PRPOApprovalsWSApp"
            },
            _content: [
              { "SAPID": user }
            ]
          }
        ]
      }
    }
    that.loadingController.create({
      spinner: 'bubbles',
      message: 'Please wait...'
    }).then((loadEl) => {
      loadEl.present();
      that.heroService.testService123(dataObj, function (err, response) {
        loadEl.dismiss();
        debugger;
        if (refresher != undefined) {
          refresher.target.complete();
        }
        if (response) {
          let obj = $.cordys.json.findObjects(response, "getPendingCounts");
          console.log("check search obj: ", obj);
          if (obj != undefined) {
            that._poCount = obj[0].getPendingCounts[0].tuple[0].PO_COUNT[0];
            that._prCount = obj[0].getPendingCounts[0].tuple[0].PR_COUNT[0];
          }
        } else {
          console.log("error found in err tag: ", err)
        }
      })
    })
  }

  doRefresh(refresher) {
    this.getPending_counts(refresher);
    // setTimeout(() => {
    //   refresher.complete();
    // }, 200);
  }

  prlist() {
    // let that = this;
    this.router.navigateByUrl('/pr-list');
  }

  polist() {
    // let that = this;
    this.router.navigateByUrl('/po-list');
  }

}
