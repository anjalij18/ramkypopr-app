import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController, NavController } from '@ionic/angular';
import { HeroService } from '../services/hero.service';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required])
  });
  // userData: any = {};
  constructor(
    private router: Router,
    private heroService: HeroService,
    private loadingController: LoadingController,
    private googlePlus: GooglePlus,
    private nativeStorage: NativeStorage,
    private navCtrl: NavController
  ) { }

  ngOnInit() {
  }

  // googleSignIn() {
  //   this.googlePlus.login({})
  //     .then(result => this.userData = result)
  //     .catch(err => this.userData = `Error ${JSON.stringify(err)}`);
  // }

  async doGoogleLogin() {
    debugger
    const loading = await this.loadingController.create({
      message: 'Please wait...'
    });
    this.presentLoading(loading);

    // this.googlePlus.login({
    //   'scopes': '', // optional, space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
    //   'webClientId': '169150208717-vd90t3jf9dtlntrkddffq5542pohpn2f.apps.googleusercontent.com', // optional clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
    //   'offline': true // Optional, but requires the webClientId - if set to true the plugin will also return a serverAuthCode, which can be used to grant offline access to a non-Google server
    // })
    this.googlePlus.login({})
      .then(user => {
        loading.dismiss();
        // this.userData = user;
        // console.log("userdata: ", this.userData);
        // localStorage.setItem("username", user.email);
        this.nativeStorage.setItem('google_user', {
          name: user.displayName,
          email: user.email
        })
          .then(() => {
            // this.router.navigate(["/home"]);
            this._login(user.email);
          }, error => {
            console.log(error);
          })
        loading.dismiss();
      }, err => {
        console.log(err)
        loading.dismiss();
      });
  }

  async presentLoading(loading) {
    return await loading.present();
  }

  _visiblePass: boolean = false;
  _showPassword() {
    let that = this;
    that._visiblePass = !that._visiblePass;
  }

  // _login() {
  //   let that = this;
  //   that.router.navigateByUrl('/home');
  //   that.heroService._toastrMsg("Logged in successfully.");
  // }

  _login(email) {
    let that = this;
    let dataObj = {
      _name: "SOAP:Envelope",
      _attrs: {
        "xmlns:SOAP": "http://schemas.xmlsoap.org/soap/envelope/"
      },
      _content: {
        "SOAP:Body": [
          {
            _name: "AuthenticateRamkyUser",
            _attrs: {
              "xmlns": "http://schemas.cordys.com/PRPOApprovalsWSApp"
            },
            _content: [
              { "Email_id": email }
            ]
          }
        ]
      }
    }

    that.loadingController.create({
      spinner: 'bubbles',
      message: 'Please wait...'
    }).then((loadEl) => {
      loadEl.present();
      that.heroService.testService123(dataObj, function (err, response) {
        loadEl.dismiss();
        debugger;
        if (response) {
          let obj = $.cordys.json.findObjects(response, "SAML");
          console.log("check search obj: ", obj);
          if (obj.length > 0) {
            localStorage.setItem("SAML", obj[0]);
            localStorage.setItem("username", email);
            that._getSAPUsers(email);
          } else {
            that.heroService._toastrErrorMsg("Invalid User. Please check logged in Email ID.");
            that.loadingController.create({
              message: "Logging out...",
              spinner: "bubbles"
            }).then((loadEl) => {
              loadEl.present();
              that.googlePlus.logout()
                .then((resp) => {
                  loadEl.dismiss();
                  console.log("on logout: ", resp);
                  localStorage.clear();
                  that.navCtrl.navigateRoot(['/login']);
                })
                .catch((err) => {
                  loadEl.dismiss();
                  console.log("on error: ", err);
                  that.googlePlus.trySilentLogin({})
                    .then(() => {
                      that.googlePlus.logout()
                        .then((resp) => {
                          loadEl.dismiss();
                          console.log("on logout: ", resp);
                          // this._toastrMsg(resp);
                          localStorage.clear();
                          that.navCtrl.navigateRoot(['/login']);
                        })
                        .catch((err) => {
                          loadEl.dismiss();
                          console.log("on error: ", err);
                        })
                    })
                })
            });
          }
        } else {
          console.log("error found in err tag: ", err)
        }
      })
    })
  }

  _getSAPUsers(email) {
    let that = this;
    let dataObj = {
      _name: "SOAP:Envelope",
      _attrs: {
        "xmlns:SOAP": "http://schemas.xmlsoap.org/soap/envelope/"
      },
      _content: {
        "SOAP:Body": [
          {
            _name: "GetSAPUserDetails",
            _attrs: {
              "xmlns": "http://schemas.cordys.com/default"
            },
            _content: [
              { "Email_ID": email },
              // { "Email_ID": "businessapps.appworks@ramky.com" },
              { "System": "PST" }
            ]
          }
        ]
      }
    }

    that.loadingController.create({
      spinner: 'bubbles',
      message: 'Please wait...'
    }).then((loadEl) => {
      loadEl.present();
      that.heroService.testService(dataObj, function (err, response) {
        loadEl.dismiss();
        debugger;
        if (response) {
          let obj = $.cordys.json.findObjects(response, "SapIDInfo");
          console.log("check SAP Users: ", obj);
          // alert("check SAP users: "+ JSON.stringify(obj))
          if (obj.length > 0) {
            let tempArray = [];
            let tempIds;

            for (let i = 0; i < obj.length; i++) {
              tempArray.push(obj[i].SapID[0]);
            }
            console.log("check sap ids: ", tempArray)
            if (tempArray.length > 0) {
              tempIds = (tempArray.join()).replace(/,/g, ';');
            }

            console.log("joined sap ids: ", tempIds);
            // alert("check SAP IDS: "+ tempIds);

            localStorage.setItem("SAPIDS", tempIds);

            that.router.navigateByUrl('/home');
            that.heroService._toastrMsg("Logged in successfully.");
          } else {
            let newObj = $.cordys.json.findObjects(response, "GetSAPUserDetailsResponse")
            if (newObj != undefined) {
              console.log("newobj: ", newObj);
              if (newObj.length > 0) {
                let tempId;
                tempId = newObj[0].SapID[0]._;
                console.log("check sap id: ", tempId);
                localStorage.setItem("SAPIDS", tempId);
                that.router.navigateByUrl('/home');
                that.heroService._toastrMsg("Logged in successfully.");
              } else {
                that.heroService._toastrErrorMsg("No SAP IDs mapped against this Email Id");
                that.loadingController.create({
                  message: "Logging out...",
                  spinner: "bubbles"
                }).then((loadEl) => {
                  loadEl.present();
                  that.googlePlus.logout()
                    .then((resp) => {
                      loadEl.dismiss();
                      console.log("on logout: ", resp);
                      localStorage.clear();
                      that.navCtrl.navigateRoot(['/login']);
                    })
                    .catch((err) => {
                      loadEl.dismiss();
                      console.log("on error: ", err);
                      that.googlePlus.trySilentLogin({})
                        .then(() => {
                          that.googlePlus.logout()
                            .then((resp) => {
                              loadEl.dismiss();
                              console.log("on logout: ", resp);
                              localStorage.clear();
                              that.navCtrl.navigateRoot(['/login']);
                            })
                            .catch((err) => {
                              loadEl.dismiss();
                              console.log("on error: ", err);
                            })
                        })
                    })
                });
              }
            }
          }
        } else {
          console.log("error found in err tag: ", err)
        }
      })
    })
  }

}
