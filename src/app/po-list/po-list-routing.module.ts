import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PoListPage } from './po-list.page';

const routes: Routes = [
  {
    path: '',
    component: PoListPage
  },
  {
    path: 'po-details',
    loadChildren: () => import('./po-details/po-details.module').then( m => m.PoDetailsPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PoListPageRoutingModule {}
