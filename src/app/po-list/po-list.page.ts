import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { DataService } from '../services/data.service';
import { HeroService } from '../services/hero.service';
declare var $: any;

@Component({
  selector: 'app-po-list',
  templateUrl: './po-list.page.html',
  styleUrls: ['./po-list.page.scss'],
})
export class PoListPage implements OnInit {
  objSearch: any = [];
  _poListArray: any = [];
  _poListArray_search: any = [];
  searchTerm: string;

  constructor(
    private heroService: HeroService,
    private toastController: ToastController,
    private router: Router,
    private loadingController: LoadingController,
    private dataService: DataService
  ) { }

  ngOnInit() {
    this.objSearch = [
      {
        por_ponumber: "12",
        por_vendor_name: "Tester One",
        por_currency: "INR",
        por_netvalue: "287.23"
      },
      {
        por_ponumber: "11",
        por_vendor_name: "Tester Two",
        por_currency: "INR",
        por_netvalue: "287.23"
      },
      {
        por_ponumber: "10",
        por_vendor_name: "Tester Three",
        por_currency: "INR",
        por_netvalue: "287.23"
      },
      {
        por_ponumber: "13",
        por_vendor_name: "Tester Four",
        por_currency: "INR",
        por_netvalue: "287.23"
      },
      {
        por_ponumber: "15",
        por_vendor_name: "Tester Five",
        por_currency: "INR",
        por_netvalue: "287.23"
      }
    ];


  }
  ionViewDidEnter() {
    this.searchTerm = undefined;
    this.get_poList();
  }

  filterItems(ev) {
    const searchTerm = ev.target.value.toLowerCase();
    let tempArray = [];
    tempArray = this._poListArray_search.filter(item => {
      return item.PONUMBER.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });
    this._poListArray = tempArray;
  }

  onClear() {
    this._poListArray = this._poListArray_search;
    this.searchTerm = undefined;
  }

  get_poList() {
    let that = this;
    let user;
    if (localStorage.getItem("SAPIDS") != null) {
      user = localStorage.getItem("SAPIDS");
    } else {
      user = null;
    }
    let dataObj = {
      _name: "SOAP:Envelope",
      _attrs: {
        "xmlns:SOAP": "http://schemas.xmlsoap.org/soap/envelope/"
      },
      _content: {
        "SOAP:Body": [
          {
            _name: "GetListOfPOsPending",
            _attrs: {
              "xmlns": "http://schemas.cordys.com/PRPOApprovalsWSApp"
            },
            _content: [
              { "SAPID": user }
            ]
          }
        ]
      }
    }

    that.loadingController.create({
      spinner: 'bubbles',
      message: 'Please wait...'
    }).then((loadEl) => {
      loadEl.present();
      that.heroService.testService(dataObj, function (err, response) {
        loadEl.dismiss();
        debugger;
        if (response) {
          let obj = $.cordys.json.findObjects(response, "SAP_PO_RELEASE");
          console.log("check PO obj: ", obj);
          if (obj.length > 0) {
            that._poListArray = obj.map((d) => {
              if (d.NETVALUE[0].$ != undefined) {
                d.NETVALUE = null;
              } else {
                d.NETVALUE = d.NETVALUE[0];
              }
              if (d.PONUMBER[0].$ != undefined) {
                d.PONUMBER = null;
              } else {
                d.PONUMBER = d.PONUMBER[0];
              }
              if (d.VENDORNAME[0].$ != undefined) {
                d.VENDORNAME = null;
              } else {
                d.VENDORNAME = d.VENDORNAME[0];
              }
              if(d.CURRENCY[0].$ != undefined){
                d.CURRENCY = 'INR';
              } else {
                d.CURRENCY = d.CURRENCY[0];
              }
              return d;
            });
            that._poListArray_search = that._poListArray;
            console.log("_poListArray: ", that._poListArray);
          }
        } else {
          console.log("error found in err tag: ", err)
        }
      })
    })
  }

  _poDetails(item) {
    let that = this;
    // debugger
    // console.log("check selected item: ", item)
    that.dataService.changeData(item);
    that.router.navigateByUrl('/po-list/po-details');
  }

}
