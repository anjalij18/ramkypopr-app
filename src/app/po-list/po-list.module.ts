import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PoListPageRoutingModule } from './po-list-routing.module';

import { PoListPage } from './po-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PoListPageRoutingModule
  ],
  declarations: [PoListPage]
})
export class PoListPageModule {}
