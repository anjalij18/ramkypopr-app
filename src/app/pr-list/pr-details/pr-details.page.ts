import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { AlertController, LoadingController } from '@ionic/angular';
import { first } from 'rxjs/operators';
import { DataService } from 'src/app/services/data.service';
import { HeroService } from 'src/app/services/hero.service';
import { URLS } from 'src/app/services/urls';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
declare var $: any;

@Component({
  selector: 'app-pr-details',
  templateUrl: './pr-details.page.html',
  styleUrls: ['./pr-details.page.scss'],
})
export class PrDetailsPage implements OnInit {
  allData: any = {};
  _lineDetails: any = [];
  _headerDetails: any = {};
  showDiv: boolean = false;
  _addedCommts: any = "";

  constructor(
    public dataService: DataService,
    public heroService: HeroService,
    private loadingController: LoadingController,
    private urls: URLS,
    private alertController: AlertController,
    private router: Router,
    private http: HTTP,
    private iab: InAppBrowser
  ) { }

  ngOnInit() {
    this.dataService.currentData.pipe(first()).subscribe((data) => {
      let data1 = data;
      console.log("Transfered pr object: ", data1);
      if (data1 != undefined) {
        this._get_prDetails(data1);
      }
    });
  }

  _get_prDetails(item) {
    let that = this;

    let dataObj = {
      _name: "SOAP:Envelope",
      _attrs: {
        "xmlns:SOAP": "http://schemas.xmlsoap.org/soap/envelope/"
      },
      _content: {
        "SOAP:Body": [
          {
            _name: "PreparexmlforPRpdf",
            _attrs: {
              "xmlns": "http://schemas.cordys.com/PRPOApprovalsWSApp"
            },
            _content: [
              {
                "PRNumber": item.PRNUMBER,
                "Source": "Mobile"
              }
            ]
          }
        ]
      }
    }

    that.loadingController.create({
      spinner: 'bubbles',
      message: 'Please wait...'
    }).then((loadEl) => {
      loadEl.present();
      that.heroService.testService(dataObj, function (err, response) {
        loadEl.dismiss();
        debugger;
        if (response) {
          let s = response['SOAP:Envelope'];

          let faultString = s['SOAP:Body'][0]['SOAP:Fault'];
          if (faultString != undefined) {
            console.log("fault string: ", faultString[0].faultstring[0]._);
            // if (faultString[0].faultstring[0]._ == "Unable to bind the artifact to a SAML assertion.") {
            that.heroService._toastrErrorMsg(faultString[0].faultstring[0]._);
            return;
            // }

          }

          let obj = $.cordys.json.findObjects(response, "PRPDFData");
          console.log("check PR obj: ", obj);
          if (obj[0] != undefined) {
            that.allData = obj[0];
            console.log("allData: ", that.allData);
            that._headerDetails = that.allData.PRHeaderInfo[0];
            that._lineDetails = that.allData.PRLinesInfo[0].Item;
            that.PDF_URL = (that._headerDetails.PDF_URL ? that._headerDetails.PDF_URL[0] : undefined);
          }
        } else {
          console.log("error found in err tag: ", err)
        }
        that._getStatusForSendBackBtn(item);
      })
    });
  }
  _getStatusForSendBackBtn(item) {
    let that = this;
    let dataObj = {
      _name: "SOAP:Envelope",
      _attrs: {
        "xmlns:SOAP": "http://schemas.xmlsoap.org/soap/envelope/"
      },
      _content: {
        "SOAP:Body": [
          {
            _name: "IsSendBackAllowedForPR",
            _attrs: {
              "xmlns": "http://schemas.cordys.com/PRPOApprovalsWSApp"
            },
            _content: [
              { "prNumber": item.PRNUMBER }
            ]
          }
        ]
      }
    }

    that.loadingController.create({
      spinner: 'bubbles',
      message: 'Please wait...'
    }).then((loadEl) => {
      loadEl.present();
      that.heroService.testService(dataObj, function (err, response) {
        loadEl.dismiss();
        debugger;
        if (response) {
          let obj = $.cordys.json.findObjects(response, "IsSendBackAllowedForPRResponse");
          console.log("check PR send back obj: ", obj);
          if (obj[0] != undefined) {
            that.allData._isSendBackShow = obj[0].return[0];
            console.log("allData: ", that.allData);
          }
        } else {
          console.log("error found in err tag: ", err)
        }
      })
    })
  }

  PDF_URL: any;
  _downloadDoc() {
    console.log("Download doc executed!")
    if (this.PDF_URL != undefined && this.PDF_URL != "null") {
      window.open(this.PDF_URL, '_system');
      // this.iab.create(this.PDF_URL, '_self', 'location=no');
    } else {
      this.heroService._toastrErrorMsg("PDF URL not found!");
    }
  }

  _approve() {
    let _url = encodeURI(this.urls._url + "home/REEL/com.reel1.callbackgateway.prpogateway.wcp?TaskID=" + this._headerDetails.TASK_ID[0] + "&Decision=Approved&Comment=" + this._addedCommts + "&BTN=&Mode=Mobile");
    console.log("Approve PR URL: ", _url);
    this.alertController.create({
      header: 'Confirm!',
      message: 'Are you sure you want to proceed?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'PROCEED',
          handler: () => {
            console.log('Confirm Okay');
            this.loadingController.create({
              spinner: 'bubbles',
              message: 'Please wait...'
            }).then((loadEL) => {
              loadEL.present();
              this.http.get(_url, {}, {})
                .then(data => {
                  loadEL.dismiss();
                  let str = data.data.toString();
                  var $str1 = $(str);
                  let finalMsg = $str1.find('p').eq(0).text();
                  this.heroService._toastrMsg(finalMsg)
                  setTimeout(() => {
                    this.router.navigateByUrl('/home');
                  }, 3000);

                })
                .catch(error => {
                  loadEL.dismiss();
                  console.log(error.status);
                  console.log(error.error); // error message as string
                  console.log(error.headers);
                  this.heroService._toastrErrorMsg("Error occured while proccessing request. Please contact administrator.")

                });
            });
          }
        }
      ]
    }).then((alertEl) => {
      alertEl.present();
    })
  }

  _reject() {
    let _url = encodeURI(this.urls._url + "home/REEL/com.reel1.callbackgateway.prpogateway.wcp?TaskID=" + this._headerDetails.TASK_ID[0] + "&Decision=Rejected&Comment=" + this._addedCommts + "&BTN=&Mode=Mobile");

    this.alertController.create({
      header: 'Confirm!',
      message: 'Are you sure you want to proceed?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'PROCEED',
          handler: () => {
            console.log('Confirm Okay');
            this.loadingController.create({
              spinner: 'bubbles',
              message: 'Please wait...'
            }).then((loadEL) => {
              loadEL.present();
              this.http.get(_url, {}, {})
                .then(data => {
                  loadEL.dismiss();
                  let str = data.data.toString();
                  var $str1 = $(str);
                  let finalMsg = $str1.find('p').eq(0).text();
                  this.heroService._toastrMsg(finalMsg)
                  setTimeout(() => {
                    this.router.navigateByUrl('/home');
                  }, 3000);

                })
                .catch(error => {
                  loadEL.dismiss();
                  console.log(error.status);
                  console.log(error.error); // error message as string
                  console.log(error.headers);
                  this.heroService._toastrErrorMsg("Error occured while proccessing request. Please contact administrator.")

                });
            });
          }
        }
      ]
    }).then((alertEl) => {
      alertEl.present();
    })
  }

  _sendback() {
    let _url = encodeURI(this.urls._url + "home/REEL/com.reel1.callbackgateway.prpogateway.wcp?TaskID=" + this._headerDetails.TASK_ID[0] + "&Decision=SendBack&Comment=" + this._addedCommts + "&BTN=&Mode=Mobile");
    this.alertController.create({
      header: 'Confirm!',
      message: 'Are you sure you want to proceed?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'PROCEED',
          handler: () => {
            console.log('Confirm Okay');
            this.loadingController.create({
              spinner: 'bubbles',
              message: 'Please wait...'
            }).then((loadEL) => {
              loadEL.present();
              this.http.get(_url, {}, {})
                .then(data => {
                  loadEL.dismiss();
                  let str = data.data.toString();
                  var $str1 = $(str);
                  let finalMsg = $str1.find('p').eq(0).text();
                  this.heroService._toastrMsg(finalMsg)
                  setTimeout(() => {
                    this.router.navigateByUrl('/home');
                  }, 3000);

                })
                .catch(error => {
                  loadEL.dismiss();
                  console.log(error.status);
                  console.log(error.error); // error message as string
                  console.log(error.headers);
                  this.heroService._toastrErrorMsg("Error occured while proccessing request. Please contact administrator.")

                });
            });
          }
        }
      ]
    }).then((alertEl) => {
      alertEl.present();
    })
  }
}
