import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PrListPage } from './pr-list.page';

const routes: Routes = [
  {
    path: '',
    component: PrListPage
  },
  {
    path: 'pr-details',
    loadChildren: () => import('./pr-details/pr-details.module').then( m => m.PrDetailsPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrListPageRoutingModule {}
