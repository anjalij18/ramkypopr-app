import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { DataService } from '../services/data.service';
import { HeroService } from '../services/hero.service';
declare var $: any;

@Component({
  selector: 'app-pr-list',
  templateUrl: './pr-list.page.html',
  styleUrls: ['./pr-list.page.scss'],
})
export class PrListPage implements OnInit {
  objSearch: any = [];
  _prListArray: any = [];
  _prListArray_search:any=[];
  searchTerm: string;

  constructor(
    private heroService: HeroService,
    private toastController: ToastController,
    private router: Router,
    private loadingController: LoadingController,
    private dataService: DataService
  ) { }

  ngOnInit() {
    this.objSearch=[
      {
        PRR_PRNUMBER: "12",
        LINECOUNT: "675",
        PRR_CURRENCY: "INR",
        fracValue: "287.23"
      },
      {
        PRR_PRNUMBER: "11",
        LINECOUNT: "5676",
        PRR_CURRENCY: "INR",
        fracValue: "287.23"
      },
      {
        PRR_PRNUMBER: "10",
        LINECOUNT: "121",
        PRR_CURRENCY: "INR",
        fracValue: "287.23"
      },
      {
        PRR_PRNUMBER: "13",
        LINECOUNT: "433",
        PRR_CURRENCY: "INR",
        fracValue: "287.23"
      },
      {
        PRR_PRNUMBER: "15",
        LINECOUNT: "122",
        PRR_CURRENCY: "INR",
        fracValue: "287.23"
      }
    ];
   
  }

  ionViewDidEnter() {
    this.searchTerm = undefined;
    this.get_prList();
  }

  filterItems(ev) {
    const searchTerm = ev.target.value.toLowerCase();
    let tempArray = [];
    tempArray = this._prListArray_search.filter(item => {
      return item.PRNUMBER.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });
    this._prListArray = tempArray;
  }

  onClear() {
    this._prListArray = this._prListArray_search;
    this.searchTerm = undefined;
  }

  get_prList() {
    let that = this;
    let user;
    if(localStorage.getItem("SAPIDS") != null) {
      user = localStorage.getItem("SAPIDS");
    } else {
      user = null;
    }
    let dataObj = {
      _name: "SOAP:Envelope",
      _attrs: {
        "xmlns:SOAP": "http://schemas.xmlsoap.org/soap/envelope/"
      },
      _content: {
        "SOAP:Body": [
          {
            _name: "GetListOfPRsPending",
            _attrs: {
              "xmlns": "http://schemas.cordys.com/PRPOApprovalsWSApp"
            },
            _content: [
              { "SAPID": user }
            ]
          }
        ]
      }
    }
   
    that.loadingController.create({
      spinner: 'bubbles',
      message: 'Please wait...'
    }).then((loadEl) => {
      loadEl.present();
      that.heroService.testService(dataObj, function (err, response) {
        loadEl.dismiss();
        debugger;
        if (response) {
            let obj = $.cordys.json.findObjects(response, "SAP_PR_RELEASE");
            console.log("check pr obj: ", obj);
            if(obj.length > 0) {
              that._prListArray = obj.map((d) => {
                if(d.NETVALUE[0].$ != undefined){
                  d.NETVALUE = null;
                } else {
                  d.NETVALUE = d.NETVALUE[0];
                }
                if(d.PRNUMBER[0].$ != undefined){
                  d.PRNUMBER = null;
                } else {
                  d.PRNUMBER = d.PRNUMBER[0];
                }
                if(d.CURRENCY[0].$ != undefined){
                  d.CURRENCY = 'INR';
                } else {
                  d.CURRENCY = d.CURRENCY[0];
                }
                return d;
              });
              that._prListArray_search = that._prListArray;
              console.log("_prListArray: ", that._prListArray);
            }
        } else {
          console.log("error found in err tag: ", err)
        }
      })
    })
  }

  _prDetails(item) {
    let that = this;
    that.dataService.changeData(item);
    that.router.navigateByUrl('/pr-list/pr-details');
  }

}
