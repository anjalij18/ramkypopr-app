import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PrListPageRoutingModule } from './pr-list-routing.module';

import { PrListPage } from './pr-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PrListPageRoutingModule
  ],
  declarations: [PrListPage]
})
export class PrListPageModule {}
