import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { URLS } from './services/urls';
import { HeroService } from './services/hero.service';
import { DataService } from './services/data.service';
import { HTTP } from '@ionic-native/http/ngx';
import { DocumentViewer } from '@ionic-native/document-viewer/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule],
  providers: [
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    URLS,
    HeroService,
    DataService,
    HTTP,
    DocumentViewer,
    GooglePlus,
    NativeStorage,
    InAppBrowser
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
