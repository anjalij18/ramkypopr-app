import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { URLS } from './urls';
import { toXML } from "jstoxml";
import * as xml2js from "xml2js";

// import * as xml2json from "xml2json";

@Injectable({
  providedIn: 'root'
})
export class HeroService {
  headers: any;
  private envelopeBuilder_: (requestBody: string) => string = null;
  constructor(public urls: URLS,
    private http: HTTP,
    private toastController: ToastController,
    public router: Router,
    public navCtrl: NavController,
    private loadingController: LoadingController,
    private googlePlus: GooglePlus) { }

  set envelopeBuilder(envelopeBuilder: (response: {}) => string) {
    this.envelopeBuilder_ = envelopeBuilder;
  }

  set envelopeBuilder2(envelopeBuilder2: (response: {}) => string) {
    this.envelopeBuilder_ = envelopeBuilder2;
  }

  testService123(input, cb: any) {
    let that = this;
    debugger
    var request: string = toXML(input);
    var envelopedRequest: string = null != this.envelopeBuilder_ ? this.envelopeBuilder_(request) : request;

    console.log(envelopedRequest);
    /////////// New code ////////////////
    let headers = {};
    let soapurl: any;
    let token: any;

    headers = {
      'Content-Type': 'text/xml;charset="utf-8"'
    };

    if (localStorage.getItem("SAML") != null) {
      token = localStorage.getItem("SAML");
      soapurl = this.urls._baseURL + '&SAMLart=' + token;
    } else {
      soapurl = this.urls._baseURL;
    }


    console.log("soapurl: " + soapurl)
    console.log("token: " + token)

    // function parseXmlToJson(xml) {
    //   const json = {};
    //   if (xml != undefined) {
    //     for (const res of xml.matchAll(/(?:<(\w*)(?:\s[^>]*)*>)((?:(?!<\1).)*)(?:<\/\1>)|<(\w*)(?:\s*)*\/>/gm)) {
    //       const key = res[1] || res[3];
    //       const value = res[2] && parseXmlToJson(res[2]);
    //       json[key] = ((value && Object.keys(value).length) ? value : res[2]) || null;

    //     }
    //   }

    //   return json;
    // }

    let xml = envelopedRequest;
    that.http.setDataSerializer('utf8');
    that.http.post(soapurl, xml, headers)
      .then((data) => {
        debugger
        // let result = parseXmlToJson(data.data);
        // cb('', result);
        xml2js.parseString(data.data, function (err, result) {
          if (result == undefined) {
            cb(err, result)
          } else {
            cb(err, result)
          }
        });
      }).catch((err) => {
        debugger
        // let result = parseXmlToJson(err.error);
        // cb('', result);
        xml2js.parseString(err.error, function (err, result) {
          if (result == undefined) {
            cb(err, result)
          } else {
            cb(err, result)
          }
        });
      })
  }

  testService(input, cb: any) {
    let that = this;
    debugger
    var request: string = toXML(input);
    var envelopedRequest: string = null != this.envelopeBuilder_ ? this.envelopeBuilder_(request) : request;

    console.log(envelopedRequest);
    /////////// New code ////////////////
    let headers = {};
    let soapurl: any;
    let token: any;

    headers = {
      'Content-Type': 'text/xml;charset="utf-8"'
    };

    if (localStorage.getItem("SAML") != null) {
      token = localStorage.getItem("SAML");
      soapurl = this.urls._baseURL + '&SAMLart=' + token;
    } else {
      soapurl = this.urls._baseURL;
    }


    console.log("soapurl: " + soapurl)
    console.log("token: " + token)

    // function parseXmlToJson(xml) {
    //   const json = {};
    //   if (xml != undefined) {
    //     for (const res of xml.matchAll(/(?:<(\w*)(?:\s[^>]*)*>)((?:(?!<\1).)*)(?:<\/\1>)|<(\w*)(?:\s*)*\/>/gm)) {
    //       const key = res[1] || res[3];
    //       const value = res[2] && parseXmlToJson(res[2]);
    //       json[key] = ((value && Object.keys(value).length) ? value : res[2]) || null;

    //     }
    //   }

    //   return json;
    // }
    // debugger
    let xml = envelopedRequest;
    that.http.setDataSerializer('utf8');
    that.http.post(soapurl, xml, headers)
      .then((data) => {
        debugger
        xml2js.parseString(data.data, function (err, result) {
          if (result == undefined) {
            cb(err, result)
          } else {
            cb(err, result)
          }
        });
      }).catch((err) => {
        debugger
        // let result = parseXmlToJson(err.error);
        // cb(err, result);
        xml2js.parseString(err, function (err, result) {
          if (result == undefined) {
            cb(err, result)
          } else {
            cb(err, result)
          }
        });
      })
  }



  _toastrMsg(msg) {
    return this.toastController.create({
      message: msg,
      duration: 3000,
      color: 'primary',
      position: 'middle'
    }).then((toastEl) => {
      toastEl.present();
    })
  }

  _toastrErrorMsg(msg) {
    return this.toastController.create({
      message: msg,
      duration: 5000,
      color: 'danger',
      position: 'middle'
    }).then((toastEl) => {
      toastEl.present();
    })
  }

  // logout() {
  //   let that = this;
  //   localStorage.clear();
  //   // localStorage.setItem('option', 'MnM');
  //   that.loadingController.create({
  //     message: "Logging out...",
  //     spinner: "bubbles"
  //   }).then((loadEl) => {
  //     loadEl.present();
  //     setTimeout(() => {
  //       loadEl.dismiss();
  //       // that.router.navigateByUrl("/login");
  //       that.navCtrl.navigateRoot(['/login']);
  //     }, 1000);
  //   });
  // }

  logout() {
    let that = this;
    that.loadingController.create({
      message: "Logging out...",
      spinner: "bubbles"
    }).then((loadEl) => {
      loadEl.present();
      this.googlePlus.logout()
        .then((resp) => {
          loadEl.dismiss();
          console.log("on logout: ", resp);
          // this._toastrMsg(resp);
          localStorage.clear();
          that.navCtrl.navigateRoot(['/login']);
        })
        .catch((err) => {
          loadEl.dismiss();
          console.log("on error: ", err);
          this.googlePlus.trySilentLogin({})
            .then(() => {
              this.googlePlus.logout()
                .then((resp) => {
                  loadEl.dismiss();
                  console.log("on logout: ", resp);
                  // this._toastrMsg(resp);
                  localStorage.clear();
                  that.navCtrl.navigateRoot(['/login']);
                })
                .catch((err) => {
                  loadEl.dismiss();
                  console.log("on error: ", err);
                  // this.googlePlus.trySilentLogin({})
                  // .then(() => {

                  // })
                })
            })
        })
    });
  }

  otoa(data) {
    return Array.isArray(data) ? data : [data];
  }
}
