export class URLS {
    _baseURL: string = "https://uatappworks.ramky.in/home/REEL/com.eibus.web.soap.Gateway.wcp?organization=o=REEL,cn=cordys,cn=defaultInst,o=reel.in" //UAT url;
    // _baseURL: string = "http://34.93.144.141:8181/home/REEL/com.eibus.web.soap.Gateway.wcp?organization=o=REEL,cn=cordys,cn=defaultInst,o=reel.in" // dev url;
    _url: string = "https://uatappworks.ramky.in/"; // for UAT
    // _url: string = "http://34.93.144.141:8181/"; // for dev
}