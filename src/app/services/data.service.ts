import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private dataSource = new BehaviorSubject<string>("");
  private detailsDataSource = new BehaviorSubject<string>("");
  currentData = this.dataSource.asObservable();
  currentData1 = this.detailsDataSource.asObservable();
  constructor() { }

  changeData(data: any) {
    this.dataSource.next(data);
  }

  removeData() {
    this.dataSource.next("");
  }

  changeIdeaDetailsData(data: any) {
    this.detailsDataSource.next(data);
  }
}
