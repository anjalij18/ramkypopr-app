import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(public router: Router) {}

  ngOnInit() {
    if (localStorage.getItem("username") != null && localStorage.getItem("SAML") != null) {
      this.router.navigateByUrl('/home');
    } else {
      if (localStorage.getItem("username") != null && localStorage.getItem("base64String") != null) {
        this.router.navigateByUrl('/home');
      } else {
        this.router.navigateByUrl('/login');
      }
    }
  }
}
