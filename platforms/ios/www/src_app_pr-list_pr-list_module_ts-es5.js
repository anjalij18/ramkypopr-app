(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (self["webpackChunkRamkyPOPR"] = self["webpackChunkRamkyPOPR"] || []).push([["src_app_pr-list_pr-list_module_ts"], {
    /***/
    32050: function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "PrListPageRoutingModule": function PrListPageRoutingModule() {
          return (
            /* binding */
            _PrListPageRoutingModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      39895);
      /* harmony import */


      var _pr_list_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./pr-list.page */
      66851);

      var routes = [{
        path: '',
        component: _pr_list_page__WEBPACK_IMPORTED_MODULE_0__.PrListPage
      }, {
        path: 'pr-details',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() */
          "src_app_pr-list_pr-details_pr-details_module_ts").then(__webpack_require__.bind(__webpack_require__,
          /*! ./pr-details/pr-details.module */
          99992)).then(function (m) {
            return m.PrDetailsPageModule;
          });
        }
      }];

      var _PrListPageRoutingModule = function PrListPageRoutingModule() {
        _classCallCheck(this, PrListPageRoutingModule);
      };

      _PrListPageRoutingModule = (0, tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule]
      })], _PrListPageRoutingModule);
      /***/
    },

    /***/
    14817: function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "PrListPageModule": function PrListPageModule() {
          return (
            /* binding */
            _PrListPageModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common */
      38583);
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/forms */
      3679);
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/angular */
      80476);
      /* harmony import */


      var _pr_list_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./pr-list-routing.module */
      32050);
      /* harmony import */


      var _pr_list_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./pr-list.page */
      66851);

      var _PrListPageModule = function PrListPageModule() {
        _classCallCheck(this, PrListPageModule);
      };

      _PrListPageModule = (0, tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule, _pr_list_routing_module__WEBPACK_IMPORTED_MODULE_0__.PrListPageRoutingModule],
        declarations: [_pr_list_page__WEBPACK_IMPORTED_MODULE_1__.PrListPage]
      })], _PrListPageModule);
      /***/
    },

    /***/
    66851: function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "PrListPage": function PrListPage() {
          return (
            /* binding */
            _PrListPage
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _raw_loader_pr_list_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! !raw-loader!./pr-list.page.html */
      78472);
      /* harmony import */


      var _pr_list_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./pr-list.page.scss */
      26710);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/router */
      39895);
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      80476);
      /* harmony import */


      var _services_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../services/data.service */
      52468);
      /* harmony import */


      var _services_hero_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../services/hero.service */
      19405);

      var _PrListPage = /*#__PURE__*/function () {
        function PrListPage(heroService, toastController, router, loadingController, dataService) {
          _classCallCheck(this, PrListPage);

          this.heroService = heroService;
          this.toastController = toastController;
          this.router = router;
          this.loadingController = loadingController;
          this.dataService = dataService;
          this.objSearch = [];
          this._prListArray = [];
          this._prListArray_search = [];
        }

        _createClass(PrListPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.objSearch = [{
              PRR_PRNUMBER: "12",
              LINECOUNT: "675",
              PRR_CURRENCY: "INR",
              fracValue: "287.23"
            }, {
              PRR_PRNUMBER: "11",
              LINECOUNT: "5676",
              PRR_CURRENCY: "INR",
              fracValue: "287.23"
            }, {
              PRR_PRNUMBER: "10",
              LINECOUNT: "121",
              PRR_CURRENCY: "INR",
              fracValue: "287.23"
            }, {
              PRR_PRNUMBER: "13",
              LINECOUNT: "433",
              PRR_CURRENCY: "INR",
              fracValue: "287.23"
            }, {
              PRR_PRNUMBER: "15",
              LINECOUNT: "122",
              PRR_CURRENCY: "INR",
              fracValue: "287.23"
            }];
          }
        }, {
          key: "ionViewDidEnter",
          value: function ionViewDidEnter() {
            this.searchTerm = undefined;
            this.get_prList();
          }
        }, {
          key: "filterItems",
          value: function filterItems(ev) {
            var searchTerm = ev.target.value.toLowerCase();
            var tempArray = [];
            tempArray = this._prListArray_search.filter(function (item) {
              return item.PRNUMBER.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
            });
            this._prListArray = tempArray;
          }
        }, {
          key: "onClear",
          value: function onClear() {
            this._prListArray = this._prListArray_search;
            this.searchTerm = undefined;
          }
        }, {
          key: "get_prList",
          value: function get_prList() {
            var that = this;
            var user;

            if (localStorage.getItem("SAPIDS") != null) {
              user = localStorage.getItem("SAPIDS");
            } else {
              user = null;
            }

            var dataObj = {
              _name: "SOAP:Envelope",
              _attrs: {
                "xmlns:SOAP": "http://schemas.xmlsoap.org/soap/envelope/"
              },
              _content: {
                "SOAP:Body": [{
                  _name: "GetListOfPRsPending",
                  _attrs: {
                    "xmlns": "http://schemas.cordys.com/PRPOApprovalsWSApp"
                  },
                  _content: [{
                    "SAPID": user
                  }]
                }]
              }
            };
            that.loadingController.create({
              spinner: 'bubbles',
              message: 'Please wait...'
            }).then(function (loadEl) {
              loadEl.present();
              that.heroService.testService(dataObj, function (err, response) {
                loadEl.dismiss();
                debugger;

                if (response) {
                  var obj = $.cordys.json.findObjects(response, "SAP_PR_RELEASE");
                  console.log("check pr obj: ", obj);

                  if (obj.length > 0) {
                    that._prListArray = obj.map(function (d) {
                      if (d.NETVALUE[0].$ != undefined) {
                        d.NETVALUE = null;
                      } else {
                        d.NETVALUE = d.NETVALUE[0];
                      }

                      if (d.PRNUMBER[0].$ != undefined) {
                        d.PRNUMBER = null;
                      } else {
                        d.PRNUMBER = d.PRNUMBER[0];
                      }

                      if (d.CURRENCY[0].$ != undefined) {
                        d.CURRENCY = 'INR';
                      } else {
                        d.CURRENCY = d.CURRENCY[0];
                      }

                      return d;
                    });
                    that._prListArray_search = that._prListArray;
                    console.log("_prListArray: ", that._prListArray);
                  }
                } else {
                  console.log("error found in err tag: ", err);
                }
              });
            });
          }
        }, {
          key: "_prDetails",
          value: function _prDetails(item) {
            var that = this;
            that.dataService.changeData(item);
            that.router.navigateByUrl('/pr-list/pr-details');
          }
        }]);

        return PrListPage;
      }();

      _PrListPage.ctorParameters = function () {
        return [{
          type: _services_hero_service__WEBPACK_IMPORTED_MODULE_3__.HeroService
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.ToastController
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.LoadingController
        }, {
          type: _services_data_service__WEBPACK_IMPORTED_MODULE_2__.DataService
        }];
      };

      _PrListPage = (0, tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-pr-list',
        template: _raw_loader_pr_list_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_pr_list_page_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
      })], _PrListPage);
      /***/
    },

    /***/
    26710: function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwci1saXN0LnBhZ2Uuc2NzcyJ9 */";
      /***/
    },

    /***/
    78472: function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"home\"></ion-back-button>\n    </ion-buttons>\n    <ion-title mode=\"ios\">PR List</ion-title>\n  </ion-toolbar>\n  <ion-toolbar mode=\"ios\">\n    <ion-searchbar mode=\"ios\" [(ngModel)]=\"searchTerm\" style=\"padding: 16px 10px 16px 10px;\" (ionInput)=\"filterItems($event)\" (ionClear)=\"onClear()\"></ion-searchbar>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <!-- <ion-list>\n    <ion-item (click)=\"prDetails(p)\" *ngFor=\"let p of objSearch\">\n      <ion-row style=\"width: 100%;\">\n        <ion-col size=\"6\" class=\"ion-no-padding\">\n          <h2>Req # {{p.PRR_PRNUMBER}}</h2>\n        </ion-col>\n        <ion-col size=\"6\" class=\"ion-no-padding ion-text-right\">\n          <h2>{{p.LINECOUNT}} Items</h2>\n        </ion-col>\n        <ion-col size=\"12\" class=\"ion-text-right\">\n          <p>\n            <span style=\"font-size: 16px; color: black;\">\n              {{p.PRR_CURRENCY}}\n            </span>&nbsp;\n            <span style=\"font-size: 18px; color: black;\">\n              {{p.fracValue}}\n            </span>\n          </p>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n  </ion-list> -->\n  <ion-card *ngFor=\"let item of _prListArray\" (click)=\"_prDetails(item)\" style=\"background-color:#EAEDED\"  mode=\"ios\">\n    <ion-card-header>\n      <ion-card-title style=\"font-size: 22px;\">Req # {{item.PRNUMBER}}</ion-card-title>\n    </ion-card-header>\n\n    <ion-card-content>{{item.VENDORNAME}}\n      <ion-row style=\"width: 100%;\">\n        <ion-col class=\"ion-no-padding ion-text-right\" size=\"12\">\n          <p>\n            <span style=\"font-size: 16px; color: black;\">{{item.CURRENCY}}</span>&nbsp;\n            <span style=\"font-size: 18px; color: black;\">\n              {{item.NETVALUE ? item.NETVALUE : 0}}\n            </span>\n          </p>\n        </ion-col>\n      </ion-row>\n    </ion-card-content>\n  </ion-card>\n  <!-- <ion-list>\n    <ion-item *ngFor=\"let item of _prListArray\">\n      <ion-row style=\"width: 100%;\">\n        <ion-col size=\"6\" class=\"ion-no-padding\">\n          <h3>Req # {{item.PRNUMBER}}</h3>\n        </ion-col>\n        <ion-col size=\"6\" class=\"ion-no-padding ion-text-right\">\n          <h2>{{p.LINECOUNT}} Items</h2>\n        </ion-col>\n        <ion-col size=\"12\" class=\"ion-text-right\">\n          <p>\n            <span style=\"font-size: 18px; color: black;\">\n              {{item.NETVALUE}}\n            </span>\n          </p>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n  </ion-list> -->\n</ion-content>\n";
      /***/
    }
  }]);
})();
//# sourceMappingURL=src_app_pr-list_pr-list_module_ts-es5.js.map