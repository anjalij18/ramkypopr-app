(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (self["webpackChunkRamkyPOPR"] = self["webpackChunkRamkyPOPR"] || []).push([["src_app_po-list_po-list_module_ts"], {
    /***/
    44309: function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "PoListPageRoutingModule": function PoListPageRoutingModule() {
          return (
            /* binding */
            _PoListPageRoutingModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      39895);
      /* harmony import */


      var _po_list_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./po-list.page */
      33043);

      var routes = [{
        path: '',
        component: _po_list_page__WEBPACK_IMPORTED_MODULE_0__.PoListPage
      }, {
        path: 'po-details',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() */
          "src_app_po-list_po-details_po-details_module_ts").then(__webpack_require__.bind(__webpack_require__,
          /*! ./po-details/po-details.module */
          94308)).then(function (m) {
            return m.PoDetailsPageModule;
          });
        }
      }];

      var _PoListPageRoutingModule = function PoListPageRoutingModule() {
        _classCallCheck(this, PoListPageRoutingModule);
      };

      _PoListPageRoutingModule = (0, tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule]
      })], _PoListPageRoutingModule);
      /***/
    },

    /***/
    80395: function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "PoListPageModule": function PoListPageModule() {
          return (
            /* binding */
            _PoListPageModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common */
      38583);
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/forms */
      3679);
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/angular */
      80476);
      /* harmony import */


      var _po_list_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./po-list-routing.module */
      44309);
      /* harmony import */


      var _po_list_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./po-list.page */
      33043);

      var _PoListPageModule = function PoListPageModule() {
        _classCallCheck(this, PoListPageModule);
      };

      _PoListPageModule = (0, tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule, _po_list_routing_module__WEBPACK_IMPORTED_MODULE_0__.PoListPageRoutingModule],
        declarations: [_po_list_page__WEBPACK_IMPORTED_MODULE_1__.PoListPage]
      })], _PoListPageModule);
      /***/
    },

    /***/
    33043: function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "PoListPage": function PoListPage() {
          return (
            /* binding */
            _PoListPage
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _raw_loader_po_list_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! !raw-loader!./po-list.page.html */
      99827);
      /* harmony import */


      var _po_list_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./po-list.page.scss */
      64847);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/router */
      39895);
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      80476);
      /* harmony import */


      var _services_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../services/data.service */
      52468);
      /* harmony import */


      var _services_hero_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../services/hero.service */
      19405);

      var _PoListPage = /*#__PURE__*/function () {
        function PoListPage(heroService, toastController, router, loadingController, dataService) {
          _classCallCheck(this, PoListPage);

          this.heroService = heroService;
          this.toastController = toastController;
          this.router = router;
          this.loadingController = loadingController;
          this.dataService = dataService;
          this.objSearch = [];
          this._poListArray = [];
          this._poListArray_search = [];
        }

        _createClass(PoListPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.objSearch = [{
              por_ponumber: "12",
              por_vendor_name: "Tester One",
              por_currency: "INR",
              por_netvalue: "287.23"
            }, {
              por_ponumber: "11",
              por_vendor_name: "Tester Two",
              por_currency: "INR",
              por_netvalue: "287.23"
            }, {
              por_ponumber: "10",
              por_vendor_name: "Tester Three",
              por_currency: "INR",
              por_netvalue: "287.23"
            }, {
              por_ponumber: "13",
              por_vendor_name: "Tester Four",
              por_currency: "INR",
              por_netvalue: "287.23"
            }, {
              por_ponumber: "15",
              por_vendor_name: "Tester Five",
              por_currency: "INR",
              por_netvalue: "287.23"
            }];
          }
        }, {
          key: "ionViewDidEnter",
          value: function ionViewDidEnter() {
            this.searchTerm = undefined;
            this.get_poList();
          }
        }, {
          key: "filterItems",
          value: function filterItems(ev) {
            var searchTerm = ev.target.value.toLowerCase();
            var tempArray = [];
            tempArray = this._poListArray_search.filter(function (item) {
              return item.PONUMBER.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
            });
            this._poListArray = tempArray;
          }
        }, {
          key: "onClear",
          value: function onClear() {
            this._poListArray = this._poListArray_search;
            this.searchTerm = undefined;
          }
        }, {
          key: "get_poList",
          value: function get_poList() {
            var that = this;
            var user;

            if (localStorage.getItem("SAPIDS") != null) {
              user = localStorage.getItem("SAPIDS");
            } else {
              user = null;
            }

            var dataObj = {
              _name: "SOAP:Envelope",
              _attrs: {
                "xmlns:SOAP": "http://schemas.xmlsoap.org/soap/envelope/"
              },
              _content: {
                "SOAP:Body": [{
                  _name: "GetListOfPOsPending",
                  _attrs: {
                    "xmlns": "http://schemas.cordys.com/PRPOApprovalsWSApp"
                  },
                  _content: [{
                    "SAPID": user
                  }]
                }]
              }
            };
            that.loadingController.create({
              spinner: 'bubbles',
              message: 'Please wait...'
            }).then(function (loadEl) {
              loadEl.present();
              that.heroService.testService(dataObj, function (err, response) {
                loadEl.dismiss();
                debugger;

                if (response) {
                  var obj = $.cordys.json.findObjects(response, "SAP_PO_RELEASE");
                  console.log("check PO obj: ", obj);

                  if (obj.length > 0) {
                    that._poListArray = obj.map(function (d) {
                      if (d.NETVALUE[0].$ != undefined) {
                        d.NETVALUE = null;
                      } else {
                        d.NETVALUE = d.NETVALUE[0];
                      }

                      if (d.PONUMBER[0].$ != undefined) {
                        d.PONUMBER = null;
                      } else {
                        d.PONUMBER = d.PONUMBER[0];
                      }

                      if (d.VENDORNAME[0].$ != undefined) {
                        d.VENDORNAME = null;
                      } else {
                        d.VENDORNAME = d.VENDORNAME[0];
                      }

                      if (d.CURRENCY[0].$ != undefined) {
                        d.CURRENCY = 'INR';
                      } else {
                        d.CURRENCY = d.CURRENCY[0];
                      }

                      return d;
                    });
                    that._poListArray_search = that._poListArray;
                    console.log("_poListArray: ", that._poListArray);
                  }
                } else {
                  console.log("error found in err tag: ", err);
                }
              });
            });
          }
        }, {
          key: "_poDetails",
          value: function _poDetails(item) {
            var that = this; // debugger
            // console.log("check selected item: ", item)

            that.dataService.changeData(item);
            that.router.navigateByUrl('/po-list/po-details');
          }
        }]);

        return PoListPage;
      }();

      _PoListPage.ctorParameters = function () {
        return [{
          type: _services_hero_service__WEBPACK_IMPORTED_MODULE_3__.HeroService
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.ToastController
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.LoadingController
        }, {
          type: _services_data_service__WEBPACK_IMPORTED_MODULE_2__.DataService
        }];
      };

      _PoListPage = (0, tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-po-list',
        template: _raw_loader_po_list_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_po_list_page_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
      })], _PoListPage);
      /***/
    },

    /***/
    64847: function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwby1saXN0LnBhZ2Uuc2NzcyJ9 */";
      /***/
    },

    /***/
    99827: function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"home\"></ion-back-button>\n    </ion-buttons>\n    <ion-title mode=\"ios\">PO List</ion-title>\n  </ion-toolbar>\n  <ion-toolbar>\n    <ion-searchbar mode=\"ios\" [(ngModel)]=\"searchTerm\" style=\"padding: 16px 10px 16px 10px;\" (ionInput)=\"filterItems($event)\" (ionClear)=\"onClear()\"></ion-searchbar>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <!-- <ion-list>\n    <ion-item *ngFor=\"let o of objSearch\" (click)=\"poDetails(o)\">\n      <ion-row style=\"width: 100%;\">\n        <ion-col size=\"12\" class=\"ion-no-padding\">\n\n          <h2>PO # {{o.por_ponumber}}</h2>\n\n          <h4 style=\"margin-top: 5%;\">\n            {{o.por_vendor_name}}</h4>\n        </ion-col>\n        <ion-col  size=\"12\" class=\"ion-no-padding ion-text-right\">\n          <p>\n            <span style=\"font-size: 16px; color: black;\">{{o.por_currency}}</span>&nbsp;\n            <span style=\"font-size: 18px; color: black;\">{{o.por_netvalue}}</span>\n            <span style=\"font-size: 18px; color: black;\">0</span>\n          </p>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n  </ion-list>-->\n  <ion-card (click)=\"_poDetails(item)\" *ngFor=\"let item of _poListArray\" style=\"background-color:#EAEDED\" mode=\"ios\">\n    <ion-card-header>\n      <ion-card-title style=\"font-size: 22px;\">PO # {{item.PONUMBER}}</ion-card-title>\n    </ion-card-header>\n\n    <ion-card-content>{{item.VENDORNAME}}\n      <ion-row style=\"width: 100%;\">\n        <ion-col class=\"ion-no-padding ion-text-right\" size=\"12\">\n          <p>\n            <span style=\"font-size: 16px; color: black;\">{{item.CURRENCY}}</span>&nbsp;\n            <span style=\"font-size: 18px; color: black;\">\n              {{item.NETVALUE ? item.NETVALUE : 0}}\n            </span>\n          </p>\n        </ion-col>\n      </ion-row>\n    </ion-card-content>\n  </ion-card>\n  <!-- <ion-list>\n    <ion-item (click)=\"_poDetails(item)\" *ngFor=\"let item of _poListArray\">\n      <ion-row style=\"width: 100%;\">\n        <ion-col class=\"ion-no-padding\" size=\"12\">\n          <h2>PO # {{item.PONUMBER}}</h2>\n\n          <h4 style=\"margin-top: 5%;\">{{item.VENDORNAME}}</h4>\n        </ion-col>\n        <ion-col class=\"ion-no-padding ion-text-right\" size=\"12\">\n          <p>\n            INR\n            <span style=\"font-size: 18px; color: black;\">\n              {{item.NETVALUE ? item.NETVALUE : 0}}\n            </span>\n          </p>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n  </ion-list> -->\n</ion-content>\n";
      /***/
    }
  }]);
})();
//# sourceMappingURL=src_app_po-list_po-list_module_ts-es5.js.map