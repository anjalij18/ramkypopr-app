(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (self["webpackChunkRamkyPOPR"] = self["webpackChunkRamkyPOPR"] || []).push([["src_app_login_login_module_ts"], {
    /***/
    45393: function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "LoginPageRoutingModule": function LoginPageRoutingModule() {
          return (
            /* binding */
            _LoginPageRoutingModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      39895);
      /* harmony import */


      var _login_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./login.page */
      66825);

      var routes = [{
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_0__.LoginPage
      }];

      var _LoginPageRoutingModule = function LoginPageRoutingModule() {
        _classCallCheck(this, LoginPageRoutingModule);
      };

      _LoginPageRoutingModule = (0, tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule]
      })], _LoginPageRoutingModule);
      /***/
    },

    /***/
    80107: function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "LoginPageModule": function LoginPageModule() {
          return (
            /* binding */
            _LoginPageModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common */
      38583);
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/forms */
      3679);
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/angular */
      80476);
      /* harmony import */


      var _login_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./login-routing.module */
      45393);
      /* harmony import */


      var _login_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./login.page */
      66825);

      var _LoginPageModule = function LoginPageModule() {
        _classCallCheck(this, LoginPageModule);
      };

      _LoginPageModule = (0, tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.ReactiveFormsModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule, _login_routing_module__WEBPACK_IMPORTED_MODULE_0__.LoginPageRoutingModule],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_1__.LoginPage]
      })], _LoginPageModule);
      /***/
    },

    /***/
    66825: function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "LoginPage": function LoginPage() {
          return (
            /* binding */
            _LoginPage
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _raw_loader_login_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! !raw-loader!./login.page.html */
      76770);
      /* harmony import */


      var _login_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./login.page.scss */
      21339);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/forms */
      3679);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/router */
      39895);
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @ionic/angular */
      80476);
      /* harmony import */


      var _services_hero_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../services/hero.service */
      19405);
      /* harmony import */


      var _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic-native/google-plus/ngx */
      19342);
      /* harmony import */


      var _ionic_native_native_storage_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic-native/native-storage/ngx */
      73885);

      var _LoginPage = /*#__PURE__*/function () {
        // userData: any = {};
        function LoginPage(router, heroService, loadingController, googlePlus, nativeStorage, navCtrl) {
          _classCallCheck(this, LoginPage);

          this.router = router;
          this.heroService = heroService;
          this.loadingController = loadingController;
          this.googlePlus = googlePlus;
          this.nativeStorage = nativeStorage;
          this.navCtrl = navCtrl;
          this.loginForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormGroup({
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormControl('', [_angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.email]),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormControl('', [_angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required])
          });
          this._visiblePass = false;
        }

        _createClass(LoginPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {} // googleSignIn() {
          //   this.googlePlus.login({})
          //     .then(result => this.userData = result)
          //     .catch(err => this.userData = `Error ${JSON.stringify(err)}`);
          // }

        }, {
          key: "doGoogleLogin",
          value: function doGoogleLogin() {
            return (0, tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var _this = this;

              var loading;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      debugger;
                      _context.next = 3;
                      return this.loadingController.create({
                        message: 'Please wait...'
                      });

                    case 3:
                      loading = _context.sent;
                      this.presentLoading(loading); // this.googlePlus.login({
                      //   'scopes': '', // optional, space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
                      //   'webClientId': '169150208717-vd90t3jf9dtlntrkddffq5542pohpn2f.apps.googleusercontent.com', // optional clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
                      //   'offline': true // Optional, but requires the webClientId - if set to true the plugin will also return a serverAuthCode, which can be used to grant offline access to a non-Google server
                      // })

                      this.googlePlus.login({}).then(function (user) {
                        loading.dismiss(); // this.userData = user;
                        // console.log("userdata: ", this.userData);
                        // localStorage.setItem("username", user.email);

                        _this.nativeStorage.setItem('google_user', {
                          name: user.displayName,
                          email: user.email
                        }).then(function () {
                          // this.router.navigate(["/home"]);
                          _this._login(user.email);
                        }, function (error) {
                          console.log(error);
                        });

                        loading.dismiss();
                      }, function (err) {
                        console.log(err);
                        loading.dismiss();
                      });

                    case 6:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "presentLoading",
          value: function presentLoading(loading) {
            return (0, tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return loading.present();

                    case 2:
                      return _context2.abrupt("return", _context2.sent);

                    case 3:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2);
            }));
          }
        }, {
          key: "_showPassword",
          value: function _showPassword() {
            var that = this;
            that._visiblePass = !that._visiblePass;
          } // _login() {
          //   let that = this;
          //   that.router.navigateByUrl('/home');
          //   that.heroService._toastrMsg("Logged in successfully.");
          // }

        }, {
          key: "_login",
          value: function _login(email) {
            var that = this;
            var dataObj = {
              _name: "SOAP:Envelope",
              _attrs: {
                "xmlns:SOAP": "http://schemas.xmlsoap.org/soap/envelope/"
              },
              _content: {
                "SOAP:Body": [{
                  _name: "AuthenticateRamkyUser",
                  _attrs: {
                    "xmlns": "http://schemas.cordys.com/PRPOApprovalsWSApp"
                  },
                  _content: [{
                    "Email_id": email
                  }]
                }]
              }
            };
            that.loadingController.create({
              spinner: 'bubbles',
              message: 'Please wait...'
            }).then(function (loadEl) {
              loadEl.present();
              that.heroService.testService123(dataObj, function (err, response) {
                loadEl.dismiss();
                debugger;

                if (response) {
                  var obj = $.cordys.json.findObjects(response, "SAML");
                  console.log("check search obj: ", obj);

                  if (obj.length > 0) {
                    localStorage.setItem("SAML", obj[0]);
                    localStorage.setItem("username", email);

                    that._getSAPUsers(email);
                  } else {
                    that.heroService._toastrErrorMsg("Invalid User. Please check logged in Email ID.");

                    that.loadingController.create({
                      message: "Logging out...",
                      spinner: "bubbles"
                    }).then(function (loadEl) {
                      loadEl.present();
                      that.googlePlus.logout().then(function (resp) {
                        loadEl.dismiss();
                        console.log("on logout: ", resp);
                        localStorage.clear();
                        that.navCtrl.navigateRoot(['/login']);
                      })["catch"](function (err) {
                        loadEl.dismiss();
                        console.log("on error: ", err);
                        that.googlePlus.trySilentLogin({}).then(function () {
                          that.googlePlus.logout().then(function (resp) {
                            loadEl.dismiss();
                            console.log("on logout: ", resp); // this._toastrMsg(resp);

                            localStorage.clear();
                            that.navCtrl.navigateRoot(['/login']);
                          })["catch"](function (err) {
                            loadEl.dismiss();
                            console.log("on error: ", err);
                          });
                        });
                      });
                    });
                  }
                } else {
                  console.log("error found in err tag: ", err);
                }
              });
            });
          }
        }, {
          key: "_getSAPUsers",
          value: function _getSAPUsers(email) {
            var that = this;
            var dataObj = {
              _name: "SOAP:Envelope",
              _attrs: {
                "xmlns:SOAP": "http://schemas.xmlsoap.org/soap/envelope/"
              },
              _content: {
                "SOAP:Body": [{
                  _name: "GetSAPUserDetails",
                  _attrs: {
                    "xmlns": "http://schemas.cordys.com/default"
                  },
                  _content: [{
                    "Email_ID": email
                  }, // { "Email_ID": "businessapps.appworks@ramky.com" },
                  {
                    "System": "PST"
                  }]
                }]
              }
            };
            that.loadingController.create({
              spinner: 'bubbles',
              message: 'Please wait...'
            }).then(function (loadEl) {
              loadEl.present();
              that.heroService.testService(dataObj, function (err, response) {
                loadEl.dismiss();
                debugger;

                if (response) {
                  var obj = $.cordys.json.findObjects(response, "SapIDInfo");
                  console.log("check SAP Users: ", obj); // alert("check SAP users: "+ JSON.stringify(obj))

                  if (obj.length > 0) {
                    var tempArray = [];
                    var tempIds;

                    for (var i = 0; i < obj.length; i++) {
                      tempArray.push(obj[i].SapID[0]);
                    }

                    console.log("check sap ids: ", tempArray);

                    if (tempArray.length > 0) {
                      tempIds = tempArray.join().replace(/,/g, ';');
                    }

                    console.log("joined sap ids: ", tempIds); // alert("check SAP IDS: "+ tempIds);

                    localStorage.setItem("SAPIDS", tempIds);
                    that.router.navigateByUrl('/home');

                    that.heroService._toastrMsg("Logged in successfully.");
                  } else {
                    var newObj = $.cordys.json.findObjects(response, "GetSAPUserDetailsResponse");

                    if (newObj != undefined) {
                      console.log("newobj: ", newObj);

                      if (newObj.length > 0) {
                        var tempId;
                        tempId = newObj[0].SapID[0]._;
                        console.log("check sap id: ", tempId);
                        localStorage.setItem("SAPIDS", tempId);
                        that.router.navigateByUrl('/home');

                        that.heroService._toastrMsg("Logged in successfully.");
                      } else {
                        that.heroService._toastrErrorMsg("No SAP IDs mapped against this Email Id");

                        that.loadingController.create({
                          message: "Logging out...",
                          spinner: "bubbles"
                        }).then(function (loadEl) {
                          loadEl.present();
                          that.googlePlus.logout().then(function (resp) {
                            loadEl.dismiss();
                            console.log("on logout: ", resp);
                            localStorage.clear();
                            that.navCtrl.navigateRoot(['/login']);
                          })["catch"](function (err) {
                            loadEl.dismiss();
                            console.log("on error: ", err);
                            that.googlePlus.trySilentLogin({}).then(function () {
                              that.googlePlus.logout().then(function (resp) {
                                loadEl.dismiss();
                                console.log("on logout: ", resp);
                                localStorage.clear();
                                that.navCtrl.navigateRoot(['/login']);
                              })["catch"](function (err) {
                                loadEl.dismiss();
                                console.log("on error: ", err);
                              });
                            });
                          });
                        });
                      }
                    }
                  }
                } else {
                  console.log("error found in err tag: ", err);
                }
              });
            });
          }
        }]);

        return LoginPage;
      }();

      _LoginPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_7__.Router
        }, {
          type: _services_hero_service__WEBPACK_IMPORTED_MODULE_2__.HeroService
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.LoadingController
        }, {
          type: _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_3__.GooglePlus
        }, {
          type: _ionic_native_native_storage_ngx__WEBPACK_IMPORTED_MODULE_4__.NativeStorage
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.NavController
        }];
      };

      _LoginPage = (0, tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_9__.Component)({
        selector: 'app-login',
        template: _raw_loader_login_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_login_page_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
      })], _LoginPage);
      /***/
    },

    /***/
    21339: function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "#formBg {\n  height: 100%;\n  width: 100%;\n}\n\n.sub_div {\n  position: absolute;\n  bottom: 0px;\n  width: 100%;\n}\n\n.formDiv {\n  padding-top: 30px;\n  padding-bottom: 30px;\n}\n\nion-icon {\n  font-size: 2em;\n}\n\nion-input {\n  font-size: 1.2em;\n}\n\n.logo {\n  background-size: contain;\n  background-repeat: no-repeat;\n  background-position: center;\n  background-color: transparent;\n  width: 45%;\n  height: 18%;\n  margin: 25% auto 7px auto;\n  text-align: center;\n}\n\n.logo img {\n  height: 160px;\n}\n\n.form-control {\n  border: none;\n  font-size: 1.25rem;\n}\n\ninput {\n  border: none;\n}\n\ninput:focus,\nselect:focus,\ntextarea:focus,\nbutton:focus {\n  outline: none;\n}\n\n.input-group {\n  border: 1.5px solid #005b9c;\n  border-radius: 5px;\n  padding: 0px;\n  background-color: white;\n}\n\n.iconC {\n  margin-top: 8px;\n  margin-left: 5px;\n  color: #005b9c;\n  font-size: 28px;\n}\n\n.iconR {\n  margin-top: 8px;\n  margin-right: 8px;\n  color: #005b9c;\n  font-size: 28px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxvZ2luLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFJQTtFQUNJLFlBQUE7RUFDQSxXQUFBO0FBSEo7O0FBVUE7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0FBUEo7O0FBVUE7RUFDSSxpQkFBQTtFQUNBLG9CQUFBO0FBUEo7O0FBU0E7RUFDSSxjQUFBO0FBTko7O0FBUUE7RUFDSSxnQkFBQTtBQUxKOztBQU9BO0VBQ0ksd0JBQUE7RUFDQSw0QkFBQTtFQUNBLDJCQUFBO0VBQ0EsNkJBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7QUFKSjs7QUFNSTtFQUNJLGFBQUE7QUFKUjs7QUFRQTtFQUVJLFlBQUE7RUFFQSxrQkFBQTtBQVBKOztBQVNBO0VBQ0ksWUFBQTtBQU5KOztBQVNBOzs7O0VBSUksYUFBQTtBQU5KOztBQVNBO0VBRUksMkJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSx1QkFBQTtBQVBKOztBQVVBO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUFQSjs7QUFTQTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FBTkoiLCJmaWxlIjoibG9naW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI2xvZ2luQmcge1xuICAgIC8vIC0tYmFja2dyb3VuZDogdXJsKFwiLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy9sb2dpbkJnLnBuZ1wiKTtcbn1cblxuI2Zvcm1CZyB7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIC8vIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvc2ltcGxlYmcucG5nXCIpO1xuICAgIC8vIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgLy8gYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAvLyBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICAvLyBwb3NpdGlvbjogZml4ZWQ7XG59XG4uc3ViX2RpdiB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvdHRvbTogMHB4O1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG4uZm9ybURpdiB7XG4gICAgcGFkZGluZy10b3A6IDMwcHg7XG4gICAgcGFkZGluZy1ib3R0b206IDMwcHg7XG59XG5pb24taWNvbiB7XG4gICAgZm9udC1zaXplOiAyZW07XG59XG5pb24taW5wdXQge1xuICAgIGZvbnQtc2l6ZTogMS4yZW07XG59XG4ubG9nbyB7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICAgIHdpZHRoOiA0NSU7XG4gICAgaGVpZ2h0OiAxOCU7XG4gICAgbWFyZ2luOiAyNSUgYXV0byA3cHggYXV0bztcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG5cbiAgICBpbWcge1xuICAgICAgICBoZWlnaHQ6IDE2MHB4O1xuICAgIH1cbn1cblxuLmZvcm0tY29udHJvbCB7XG4gICAgLy8gcGFkZGluZzogMjVweCAxMHB4O1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICAvLyBib3JkZXI6IDEuNXB4IHNvbGlkICMyYzUzNjQ7XG4gICAgZm9udC1zaXplOiAxLjI1cmVtO1xufVxuaW5wdXQge1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbn1cbmlucHV0OmZvY3VzLFxuc2VsZWN0OmZvY3VzLFxudGV4dGFyZWE6Zm9jdXMsXG5idXR0b246Zm9jdXMge1xuICAgIG91dGxpbmU6IG5vbmU7XG59XG5cbi5pbnB1dC1ncm91cCB7XG4gICAgLy8gYm9yZGVyOiAxcHggZ3JheSBzb2xpZDtcbiAgICBib3JkZXI6IDEuNXB4IHNvbGlkICMwMDViOWM7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIHBhZGRpbmc6IDBweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cblxuLmljb25DIHtcbiAgICBtYXJnaW4tdG9wOiA4cHg7XG4gICAgbWFyZ2luLWxlZnQ6IDVweDtcbiAgICBjb2xvcjogIzAwNWI5YztcbiAgICBmb250LXNpemU6IDI4cHg7XG59XG4uaWNvblIge1xuICAgIG1hcmdpbi10b3A6IDhweDtcbiAgICBtYXJnaW4tcmlnaHQ6IDhweDtcbiAgICBjb2xvcjogIzAwNWI5YztcbiAgICBmb250LXNpemU6IDI4cHg7XG59XG4vLyAucmFkaW8taXRlbSB7XG4vLyAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuLy8gICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbi8vICAgICBwYWRkaW5nOiAwIDZweDtcbi8vICAgICBtYXJnaW46IDEwcHggMCAwO1xuLy8gICB9XG5cbi8vICAgLnJhZGlvLWl0ZW0gaW5wdXRbdHlwZT0ncmFkaW8nXSB7XG4vLyAgICAgZGlzcGxheTogbm9uZTtcbi8vICAgfVxuXG4vLyAgIC5yYWRpby1pdGVtIGxhYmVsIHtcbi8vICAgICBjb2xvcjogIzJjNTM2NDtcbi8vICAgICBmb250LXdlaWdodDogYm9sZDtcbi8vICAgICBmb250LXNpemU6IDEuMjVyZW07XG4vLyAgIH1cblxuLy8gICAucmFkaW8taXRlbSBsYWJlbDpiZWZvcmUge1xuLy8gICAgIGNvbnRlbnQ6IFwiIFwiO1xuLy8gICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbi8vICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4vLyAgICAgdG9wOiA1cHg7XG4vLyAgICAgbWFyZ2luOiAwIDVweCAwIDA7XG4vLyAgICAgd2lkdGg6IDIwcHg7XG4vLyAgICAgaGVpZ2h0OiAyMHB4O1xuLy8gICAgIGJvcmRlci1yYWRpdXM6IDExcHg7XG4vLyAgICAgYm9yZGVyOiAycHggc29saWQgIzJjNTM2NDtcbi8vICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbi8vICAgfVxuXG4vLyAgIC5yYWRpby1pdGVtIGlucHV0W3R5cGU9cmFkaW9dOmNoZWNrZWQgKyBsYWJlbDphZnRlciB7XG4vLyAgICAgYm9yZGVyLXJhZGl1czogMTFweDtcbi8vICAgICB3aWR0aDogMTJweDtcbi8vICAgICBoZWlnaHQ6IDEycHg7XG4vLyAgICAgcG9zaXRpb246IGFic29sdXRlO1xuLy8gICAgIHRvcDogOXB4O1xuLy8gICAgIGxlZnQ6IDEwcHg7XG4vLyAgICAgY29udGVudDogXCIgXCI7XG4vLyAgICAgZGlzcGxheTogYmxvY2s7XG4vLyAgICAgYmFja2dyb3VuZDogIzJjNTM2NDtcbi8vICAgfVxuIl19 */";
      /***/
    },

    /***/
    76770: function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n<ion-header [translucent]=\"true\">\n  <ion-toolbar color=\"primary\">\n    <ion-title style=\"text-align: center;\">Welcome To Ramky</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content id=loginBg>\n  <div class=\"warning\">\n    <div class=\"logo\">\n      <img src=\"assets/imgs/RamkyLogo2.png\">\n    </div>\n    <div class=\"sub_div\">\n      <form [formGroup]=\"loginForm\" class=\"ion-padding\" id=\"formBg\">\n        <div class=\"formDiv\">\n          <!-- <ion-list>\n            <ion-item style=\"border: 1.5px solid #005b9c;border-radius: 10px;\">\n              <span slot=\"start\"><ion-icon name=\"person-circle-outline\" color=\"primary\"></ion-icon></span>\n              <ion-input type=\"text\" placeholder=\"Enter User Name\" formControlName=\"username\"></ion-input>\n            </ion-item>\n            <br>\n            <ion-item style=\"border: 1.5px solid #005b9c;border-radius: 10px;\">\n              <span slot=\"start\"><ion-icon name=\"lock-closed-outline\" color=\"primary\"></ion-icon></span>\n              <ion-input *ngIf=\"_visiblePass == false\" type=\"password\" placeholder=\"Enter Password\" formControlName=\"password\"></ion-input>\n              <ion-input *ngIf=\"_visiblePass == true\" type=\"text\" placeholder=\"Enter Password\" formControlName=\"password\"></ion-input>\n              <span slot=\"end\">\n                <ion-icon name=\"eye-off-outline\" color=\"primary\" *ngIf=\"_visiblePass == false\" (click)=\"_showPassword()\"></ion-icon>\n                <ion-icon name=\"eye-outline\" color=\"primary\" *ngIf=\"_visiblePass == true\" (click)=\"_showPassword()\"></ion-icon>\n              </span>\n            </ion-item>\n          </ion-list>\n          \n          <br>\n       \n          <ion-button expand=\"block\" color=\"primary\" size=\"large\" (click)=\"_login()\"\n            [disabled]=\"!loginForm.valid\">Login</ion-button> -->\n          <ion-button expand=\"block\" color=\"danger\" size=\"large\" (click)=\"doGoogleLogin()\">Login with Google</ion-button>\n        </div>\n      </form>\n    </div>\n  </div>\n\n</ion-content>";
      /***/
    }
  }]);
})();
//# sourceMappingURL=src_app_login_login_module_ts-es5.js.map